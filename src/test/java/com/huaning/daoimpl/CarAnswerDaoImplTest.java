package com.huaning.daoimpl;

import com.huaning.dao.CarAnswerDao;
import com.huaning.dao.CarQuestionDao;
import com.huaning.pojo.Car;
import com.huaning.pojo.CarAnswer;
import com.huaning.pojo.CarQuestion;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarAnswerDaoImplTest {

    private static CarAnswerDao carAnswerDao;
    private static CarQuestionDao carQuestionDao;

    @BeforeClass
    public static void init(){
        carAnswerDao = new CarAnswerDaoImpl();
        carQuestionDao = new CarQuestionDaoImpl();
    }

    @Test
    public void addCarAnswer() throws Exception {
        carQuestionDao.addQuestion(new CarQuestion("hello"));
        CarQuestion carQuestionByKeyword = carQuestionDao.getQuestionByKeyword("hello");
        CarQuestion carQuestionById = carQuestionDao.getQuestionById(carQuestionByKeyword.getId());
        assertEquals(carQuestionById, carQuestionByKeyword);
        carAnswerDao.addCarAnswer(new CarAnswer(carQuestionByKeyword.getId(), "answer"));
        List<CarAnswer> answerList = carAnswerDao.getCarAnswersByQuestionId(carQuestionByKeyword.getId());
        assertEquals("answer", answerList.get(0).getAnswer());
        carAnswerDao.deleteCarAnswerByQuestionId(carQuestionByKeyword.getId());
        carQuestionDao.deleteCarQuestionById(carQuestionByKeyword.getId());
    }

    @Test
    public void getCarAnswerById() throws Exception {

    }

    @Test
    public void getCarAnswersByQuestionId() throws Exception {

    }

    @Test
    public void deleteCarAnswerById() throws Exception {

    }

    @Test
    public void deleteCarAnswerByQuestionId() throws Exception {

    }

    @Test
    public void updateCarAnswer() throws Exception {

    }

}