package com.huaning.daoimpl;

import com.huaning.dao.CarModelDao;
import com.huaning.pojo.CarModel;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarModelDaoImplTest {

    private static CarModelDao carModelDao;

    @BeforeClass
    public static void init(){
        carModelDao = new CarModelDaoImpl();
    }

    @Test
    public void changeResultTOList() throws Exception {
        carModelDao.addCarModel(new CarModel(4, "a4", "1.5L"));
        CarModel carModel = carModelDao.getCarModelByName("a4");
        assertEquals("a4", carModel.getName());
        carModelDao.deleteCarModelById(carModel.getId());
    }

    @Test
    public void addCarModel() throws Exception {

    }

    @Test
    public void getCarModelById() throws Exception {

    }

    @Test
    public void getCarModelByName() throws Exception {

    }

    @Test
    public void getCarModelsByBrandId() throws Exception {

    }

    @Test
    public void deleteCarModelById() throws Exception {

    }

    @Test
    public void deleteCarModelByName() throws Exception {

    }

    @Test
    public void deleteCarModelByBrandId() throws Exception {

    }

    @Test
    public void updateCarModel() throws Exception {

    }

}