package com.huaning.daoimpl;

import com.huaning.dao.NewsDao;
import com.huaning.pojo.News;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by disneyland on 7/13/16.
 */

public class NewsDaoImplTest {

    private static NewsDao newsDao;

    @BeforeClass
    public static void init(){
        newsDao = new NewsDaoImpl();
    }


    @Test
    public void addNews() throws Exception {
        newsDao.addNews(new News("hello", "content", "source"));
        List<News> list = newsDao.getNewsByKeyword("hello");
        assertEquals("hello", list.get(0).getTitle());
        newsDao.deleteNewsById(list.get(0).getId());
    }

    @Test
    public void getNewsById() throws Exception {

    }

    @Test
    public void getNewsByKeyword() throws Exception {

    }

    @Test
    public void getMostViewdNews() throws Exception {

    }

    @Test
    public void getLatestNews() throws Exception {

    }

    @Test
    public void deleteNewsById() throws Exception {

    }

    @Test
    public void updateNews() throws Exception {

    }

}