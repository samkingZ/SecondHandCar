package com.huaning.daoimpl;

import com.huaning.dao.CarBrandDao;
import com.huaning.pojo.CarBrand;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarBrandDaoImplTest {
    private static CarBrandDao carBrandDao;

    @BeforeClass
    public static void init(){
        carBrandDao = new CarBrandDaoImpl();
    }

    @Test
    public void addCarBrand() throws Exception {
        carBrandDao.addCarBrand(new CarBrand("toyoto"," japan", "ok"));
        CarBrand carBrandByName = carBrandDao.getCarBrandByName("toyoto");
        CarBrand carBrandById = carBrandDao.getCarBrandById(carBrandByName.getId());
        assertEquals(carBrandById, carBrandByName);
        carBrandDao.deleteCarBrandById(carBrandById.getId());

    }

    @Test
    public void getCarBrandById() throws Exception {

    }

    @Test
    public void getCarBrandByName() throws Exception {

    }

    @Test
    public void deleteCarBrandById() throws Exception {

    }

    @Test
    public void deleteCarBrandByName() throws Exception {

    }

    @Test
    public void updateCarBrand() throws Exception {

    }

    @Test
    public void getCarBrandsBySql() throws Exception {

    }

    @Test
    public void getAllCarBrands() throws Exception {

    }

}