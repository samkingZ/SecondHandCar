package com.huaning.daoimpl;

import com.huaning.dao.AdminDao;
import com.huaning.pojo.Admin;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by disneyland on 7/12/16.
 */
public class AdminDaoImplTest {

    private static AdminDao adminDao;

    @BeforeClass
    public static void init(){
        adminDao = new AdminDaoImpl();
    }

    @Test
    public void addAdmin() throws Exception {
        adminDao.addAdmin(new Admin(
                "account", "password", "name",
                "telphone", "mail"
                ));
        Admin admin = adminDao.getAdminByAccount("account");
        assertEquals("account", admin.getAccount());
        adminDao.deleteAdminByAccount("account");
    }

    @Test
    public void getAdminById() throws Exception {
        adminDao.addAdmin(new Admin(
                "account", "password", "name",
                "telphone", "mail"
                ));
        Admin adminByAccount = adminDao.getAdminByAccount("account");
        Admin adminById = adminDao.getAdminById(adminByAccount.getId());
        assertEquals(adminByAccount, adminById);
        adminDao.deleteAdminByAccount("account");
    }

    @Test
    public void getAdminByAccount() throws Exception {

    }

    @Test
    public void deleteAdminById() throws Exception {

    }

    @Test
    public void deleteAdminByAccount() throws Exception {

    }

    @Test
    public void updateAdmin() throws Exception {
        adminDao.addAdmin(new Admin(
                "account", "password", "name",
                "telphone", "mail"
                ));
        Admin adminByAccount = adminDao.getAdminByAccount("account");
        adminByAccount.setAccount("hello");
        adminDao.updateAdmin(adminByAccount.getId(), adminByAccount);
        assertEquals(adminDao.getAdminById(adminByAccount.getId()).getAccount(), "hello");
        adminDao.deleteAdminById(adminByAccount.getId());
    }

}