package com.huaning.daoimpl;

import com.huaning.dao.UserDao;
import com.huaning.pojo.User;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by disneyland on 7/12/16.
 */
public class UserDaoImplTest {

    private static UserDao userDao;

    @BeforeClass
    public static void init(){
        userDao = new UserDaoImpl();
    }

    @Test
    public void addUser() throws Exception {
        userDao.addUser(new User("account", "password", "name", "telphone", "mail"));
        User user = userDao.getUserByAccount("account");
        assertEquals("account", user.getAccount());
        userDao.deleteUserById(user.getId());
    }

    @Test
    public void getUserById() throws Exception {
        userDao.addUser(new User("account", "password", "name", "telphone", "mail"));
        User userByAccount = userDao.getUserByAccount("account");
        User userById = userDao.getUserById(userByAccount.getId());
        assertEquals(userByAccount.getName(), userById.getName());
        userDao.deleteUserByAccount("account");
    }

    @Test
    public void getUserByAccount() throws Exception {

    }

    @Test
    public void deleteUserById() throws Exception {

    }

    @Test
    public void deleteUserByAccount() throws Exception {

    }

    @Test
    public void updateUserById() throws Exception {

        userDao.addUser(new User("account", "password", "name", "telphone", "mail"));
        User userByAccount = userDao.getUserByAccount("account");
        userByAccount.setAccount("hello");
        userDao.updateUserById(userByAccount.getId(), userByAccount);
        User userById = userDao.getUserById(userByAccount.getId());
        assertEquals("hello", userById.getAccount());
        userDao.deleteUserById(userById.getId());
    }

    @Test
    public void updateUserByAccount() throws Exception {

    }

    @Test
    public void getUsersBySql() throws Exception {

    }

}