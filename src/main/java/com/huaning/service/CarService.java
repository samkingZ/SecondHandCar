package com.huaning.service;

import com.huaning.dao.CarDao;
import com.huaning.dao.SecondCarDao;
import com.huaning.daoimpl.CarDaoImpl;
import com.huaning.daoimpl.SecondCarDaoImpl;
import com.huaning.pojo.Car;

/**
 * Created by disneyland on 7/13/16.
 */
public interface CarService {

    Car getCarBySecondCarId(int id);

    Car addCar(Car car);

    Car deleteCar(int id);
}
