package com.huaning.service;

import com.huaning.pojo.SecondCar;
import com.huaning.pojo.User;

/**
 * Created by disneyland on 7/13/16.
 */
public interface UserService {
    boolean login(String account, String password);

    boolean register(User user);

    User getUserByAccount(String account);

    void sellCar(SecondCar secondCar);

    void getSellingCar(int id);
}
