package com.huaning.service;

import com.huaning.pojo.CarAnswer;

import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public interface CarAnswerService {
    List<CarAnswer> getCarAnswerByCarQuestionId(int carQuestionId);
}
