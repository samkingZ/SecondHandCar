package com.huaning.service;

import com.huaning.pojo.*;

/**
 * Created by disneyland on 7/13/16.
 */
public interface AdminService {


    boolean checkLogin(String account, String password);

    boolean register(Admin admin);

    void addNews(News news);

    void deleteNews(int id);

    void addCarBrand(CarBrand brand);

    void deleteCarBrand(int id);

    void addCarModel(CarModel model);

    void deleteCarMode(int id);

    void addCar(Car car);

    void deleteCar(int id);


}
