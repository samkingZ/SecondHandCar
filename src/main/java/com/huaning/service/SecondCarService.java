package com.huaning.service;

import com.huaning.pojo.SecondCar;
import com.sun.org.apache.bcel.internal.generic.LSTORE;

import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public interface SecondCarService {

    SecondCar getSecondCarBySecondCarId(int id);

    int getPulishedCarNum();

    List<SecondCar> getLatestPublishedCar(int num);

    List<SecondCar> getLatestPublishedCarByPage(int index, int num);

    List<SecondCar> getSecondCarByBrand(String brand, List<SecondCar> secondCarList);

    List<SecondCar> getSecondCarByBrand(String brand);

    List<SecondCar> getSecondCarByModel(String model, List<SecondCar> secondCarList);

    List<SecondCar> getSecondCarByModel(String model);

    List<SecondCar> getSecondCarByPriceRange(int low, int high, List<SecondCar> secondCarList);

    List<SecondCar> getSecondCarByPriceRange(int low, int high);

    List<SecondCar> getSecondCarByMileageRange(int low, int high, List<SecondCar> secondCarList);

    List<SecondCar> getSecondCarByMileageRange(int low, int high);

    void addSecondCar(SecondCar secondCar);

}
