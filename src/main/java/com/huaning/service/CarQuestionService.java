package com.huaning.service;

import com.huaning.pojo.CarQuestion;

import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public interface CarQuestionService {
    List<CarQuestion> getMostPopularQuestion(int num);
}
