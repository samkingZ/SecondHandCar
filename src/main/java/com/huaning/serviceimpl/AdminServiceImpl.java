package com.huaning.serviceimpl;

import com.huaning.dao.*;
import com.huaning.daoimpl.*;
import com.huaning.pojo.*;
import com.huaning.service.AdminService;

/**
 * Created by disneyland on 7/13/16.
 */
public class AdminServiceImpl implements AdminService{
    private AdminDao adminDao = new AdminDaoImpl();
    private SecondCarDao secondCarDao = new SecondCarDaoImpl();
    private NewsDao newsDao = new NewsDaoImpl();
    private CarBrandDao carBrandDao = new CarBrandDaoImpl();
    private CarModelDao carModelDao = new CarModelDaoImpl();
    private CarDao carDao = new CarDaoImpl();

    @Override
    public boolean checkLogin(String account, String password) {
        Admin admin = adminDao.getAdminByAccount(account);
        if(admin!=null && admin.getPassword().equals(password)){
            return true;
        }
        return false;
    }

    @Override
    public boolean register(Admin admin) {
        Admin registeredAdmin = adminDao.getAdminByAccount(admin.getAccount());
        if(registeredAdmin==null || !registeredAdmin.getAccount().equals(admin.getAccount())){
            adminDao.addAdmin(admin);
            return true;
        }
        return false;
    }

    @Override
    public void addNews(News news) {
        newsDao.addNews(news);
    }

    @Override
    public void deleteNews(int id) {
        newsDao.deleteNewsById(id);
    }

    @Override
    public void addCarBrand(CarBrand brand) {
        carBrandDao.addCarBrand(brand);
    }

    @Override
    public void deleteCarBrand(int id) {
        carBrandDao.deleteCarBrandById(id);
    }

    @Override
    public void addCarModel(CarModel model) {
        carModelDao.addCarModel(model);
    }

    @Override
    public void deleteCarMode(int id) {
        carModelDao.deleteCarModelById(id);
    }

    @Override
    public void addCar(Car car) {
        carDao.addCar(car);
    }

    @Override
    public void deleteCar(int id) {
        carDao.deleteCarById(id);
    }
}
