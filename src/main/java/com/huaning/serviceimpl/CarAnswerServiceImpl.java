package com.huaning.serviceimpl;

import com.huaning.dao.CarAnswerDao;
import com.huaning.daoimpl.CarAnswerDaoImpl;
import com.huaning.pojo.CarAnswer;
import com.huaning.service.CarAnswerService;

import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarAnswerServiceImpl implements CarAnswerService{

    private CarAnswerDao carAnswerDao = new CarAnswerDaoImpl();

    @Override
    public List<CarAnswer> getCarAnswerByCarQuestionId(int carQuestionId) {
        return carAnswerDao.getCarAnswersByQuestionId(carQuestionId);
    }
}
