package com.huaning.serviceimpl;

import com.huaning.dao.CarAnswerDao;
import com.huaning.dao.CarQuestionDao;
import com.huaning.daoimpl.CarAnswerDaoImpl;
import com.huaning.daoimpl.CarQuestionDaoImpl;
import com.huaning.pojo.Car;
import com.huaning.pojo.CarQuestion;
import com.huaning.service.CarQuestionService;

import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarQuestionServiceImpl implements CarQuestionService {
    private CarQuestionDao carQuestionDao = new CarQuestionDaoImpl();
    private CarAnswerDao carAnswerDao = new CarAnswerDaoImpl();

    @Override
    public List<CarQuestion> getMostPopularQuestion(int num) {
        return carQuestionDao.getMostPopularQuestion(num);
    }

    public List<CarQuestion> getLatestQuestion(int num){
       return carQuestionDao.getLatestQuestion(num);
    }
}
