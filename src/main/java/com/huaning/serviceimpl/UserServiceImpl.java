package com.huaning.serviceimpl;

import com.huaning.dao.SecondCarDao;
import com.huaning.dao.UserDao;
import com.huaning.daoimpl.SecondCarDaoImpl;
import com.huaning.daoimpl.UserDaoImpl;
import com.huaning.pojo.SecondCar;
import com.huaning.pojo.User;
import com.huaning.service.UserService;

/**
 * Created by disneyland on 7/13/16.
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();
    private SecondCarDao secondCarDao = new SecondCarDaoImpl();

    @Override
    public boolean login(String account, String password) {
        User user = userDao.getUserByAccount(account);
        if(user !=null && user.getPassword().equals(password)){
            return true;
        }
        return false;
    }

    @Override
    public boolean register(User user) {
        User registerUser = userDao.getUserByAccount(user.getAccount());
        if(registerUser==null || !registerUser.getAccount().equals(user.getAccount())){
            userDao.addUser(user);
            return true;
        }
        return false;
    }

    @Override
    public User getUserByAccount(String account) {
       return userDao.getUserByAccount(account);
    }

    @Override
    public void sellCar(SecondCar secondCar){
        secondCarDao.addSecondCar(secondCar);
    }

    @Override
    public void getSellingCar(int id) {
        secondCarDao.getSecondCarByUserId(id);
    }
}
