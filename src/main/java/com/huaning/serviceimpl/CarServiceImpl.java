package com.huaning.serviceimpl;

import com.huaning.dao.CarDao;
import com.huaning.dao.SecondCarDao;
import com.huaning.daoimpl.CarDaoImpl;
import com.huaning.daoimpl.SecondCarDaoImpl;
import com.huaning.pojo.Car;
import com.huaning.service.CarService;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarServiceImpl implements CarService {
    private CarDao carDao = new CarDaoImpl();
    private SecondCarDao secondCarDao = new SecondCarDaoImpl();

    @Override
    public Car getCarBySecondCarId(int id) {
        return null;
    }

    @Override
    public Car addCar(Car car) {
        return null;
    }

    @Override
    public Car deleteCar(int id) {
        return null;
    }
}
