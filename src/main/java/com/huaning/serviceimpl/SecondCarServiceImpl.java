package com.huaning.serviceimpl;

import com.huaning.dao.CarBrandDao;
import com.huaning.dao.CarDao;
import com.huaning.dao.CarModelDao;
import com.huaning.dao.SecondCarDao;
import com.huaning.daoimpl.CarBrandDaoImpl;
import com.huaning.daoimpl.CarDaoImpl;
import com.huaning.daoimpl.CarModelDaoImpl;
import com.huaning.daoimpl.SecondCarDaoImpl;
import com.huaning.pojo.Car;
import com.huaning.pojo.CarBrand;
import com.huaning.pojo.CarModel;
import com.huaning.pojo.SecondCar;
import com.huaning.service.SecondCarService;

import java.util.*;

/**
 * Created by disneyland on 7/13/16.
 */
public class SecondCarServiceImpl implements SecondCarService {

    private SecondCarDao secondCarDao = new SecondCarDaoImpl();
    private CarBrandDao carBrandDao = new CarBrandDaoImpl();
    private CarModelDao carModelDao = new CarModelDaoImpl();
    private CarDao carDao = new CarDaoImpl();

    @Override
    public SecondCar getSecondCarBySecondCarId(int id) {
        return secondCarDao.getSecondCarById(id);
    }

    public List<SecondCar> getLatestPublishedCar(int num){
        return secondCarDao.getLatestPublishedCar(0,12);
    }

    public List<SecondCar> getLatestPublishedCarByPage(int index, int num){
        return secondCarDao.getLatestPublishedCar(index, index+num);
    }

    public int getPulishedCarNum(){
        return secondCarDao.getPulishedCarNum();
    }

    @Override
    public List<SecondCar> getSecondCarByBrand(String brand, List<SecondCar> secondCarList) {
        CarBrand carBrand = carBrandDao.getCarBrandByName(brand);
        List<Car> carList = carDao.getCarByCarBrandId(carBrand.getId());
        Set<Integer> carIdSet = new HashSet<Integer>();
        for(Car car : carList){
            carIdSet.add(car.getId());
        }
        List<SecondCar> simpySecondCarList = new LinkedList<SecondCar>(secondCarList);
        for(int i=0; i<simpySecondCarList.size(); i++){
            if(!carIdSet.contains(simpySecondCarList.get(i))){
                simpySecondCarList.remove(i);
            }
        }
        return simpySecondCarList;
    }

    @Override
    public List<SecondCar> getSecondCarByBrand(String brand) {
        CarBrand carBrand = carBrandDao.getCarBrandByName(brand);
        List<Car> carList = carDao.getCarByCarBrandId(carBrand.getId());
        List<SecondCar> secondCarList = new LinkedList<SecondCar>();
        for(Car car : carList){
            secondCarList.addAll(secondCarDao.getSecondCarByCarId(car.getId()));
        }
        return secondCarList;
    }

    @Override
    public List<SecondCar> getSecondCarByModel(String model, List<SecondCar> secondCarList) {
        List<Car> carList = carDao.getCarByCarModelId(carModelDao.getCarModelByName(model).getId());
        List<SecondCar> simplySecondCarList = new LinkedList<SecondCar>(secondCarList);
        Set<Integer> carIdSet = new HashSet<Integer>();
        for(Car car : carList){
            carIdSet.add(car.getId());
        }
        for(int i=0; i<simplySecondCarList.size(); i++){
            if(!carIdSet.contains(simplySecondCarList.get(i).getCarId())){
                simplySecondCarList.remove(i);
            }
        }
        return simplySecondCarList;
    }

    @Override
    public List<SecondCar> getSecondCarByModel(String model) {
        List<Car> carList = carDao.getCarByCarModelId(carModelDao.getCarModelByName(model).getId());
        List<SecondCar> secondCarList = new LinkedList<SecondCar>();
        for(Car car : carList){
            secondCarList.addAll(secondCarDao.getSecondCarByCarId(car.getId()));
        }
        return secondCarList;
    }

    @Override
    public List<SecondCar> getSecondCarByPriceRange(int low, int high, List<SecondCar> secondCarList) {
        List<SecondCar> simplySecondCarList = new LinkedList<SecondCar>(secondCarList);
        for(int i=0; i<simplySecondCarList.size(); i++){
            int price = simplySecondCarList.get(i).getPrice();
            if(price<low || price>high){
                simplySecondCarList.remove(i);
            }
        }
        return simplySecondCarList;
    }

    @Override
    public List<SecondCar> getSecondCarByPriceRange(int low, int high) {
        return secondCarDao.getSecondCarByPriceRange(low, high);
    }

    @Override
    public List<SecondCar> getSecondCarByMileageRange(int low, int high, List<SecondCar> secondCarList) {
        List<SecondCar> simplySecondCarList = new LinkedList<SecondCar>(secondCarList);
        for(int i=0; i<simplySecondCarList.size(); i++){
            int mileage = simplySecondCarList.get(i).getMileage();
            if(mileage<low || mileage>high){
                simplySecondCarList.remove(i);
            }
        }
        return simplySecondCarList;
    }

    @Override
    public List<SecondCar> getSecondCarByMileageRange(int low, int high) {
        return secondCarDao.getSecondCarByMileageRange(low, high);
    }

    public  SecondCar getSeconCarById(int id){
        return secondCarDao.getSecondCarById(id);
    }

    @Override
    public void addSecondCar(SecondCar secondCar) {
        secondCarDao.addSecondCar(secondCar);
    }


}
