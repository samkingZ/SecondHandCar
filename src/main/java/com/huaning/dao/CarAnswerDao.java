package com.huaning.dao;

import com.huaning.pojo.CarAnswer;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface CarAnswerDao {
    void addCarAnswer(CarAnswer carAnswer);

    CarAnswer getCarAnswerById(int id);

    List<CarAnswer> getCarAnswersByQuestionId(int id);

    void deleteCarAnswerById(int id);

    void deleteCarAnswerByQuestionId(int id);

    CarAnswer updateCarAnswer(int id, CarAnswer carAnswer);
}
