package com.huaning.dao;

import com.huaning.util.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public abstract class BaseDao {
    protected abstract List changeResultTOList(ResultSet result);

    public void excuteUpdate(String sql, Object[] args){
        Connection conn = JdbcUtil.getConnection();
        PreparedStatement preStat = JdbcUtil.getPreparedStatement(conn, sql);
        JdbcUtil.excuteUpdate(preStat, args);
        JdbcUtil.releaseStatement(preStat);
        JdbcUtil.releaseConnection(conn);
    }

    public List executeQuery(String sql, Object[] args){
        Connection conn = JdbcUtil.getConnection();
        PreparedStatement preStat = JdbcUtil.getPreparedStatement(conn, sql);
        ResultSet result = JdbcUtil.executeQuery(preStat, args);
        List list = changeResultTOList(result);
        JdbcUtil.releaseResultSet(result);
        JdbcUtil.releaseStatement(preStat);
        JdbcUtil.releaseConnection(conn);
        return  list;
    }

    public List executeQuery(String sql){
        return executeQuery(sql, null);
    }
}
