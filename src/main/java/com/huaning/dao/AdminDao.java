package com.huaning.dao;

import com.huaning.pojo.Admin;

/**
 * Created by disneyland on 7/12/16.
 */
public interface AdminDao {
    void addAdmin(Admin admin);

    Admin getAdminById(int id);

    Admin getAdminByAccount(String account);

    void deleteAdminById(int id);

    void deleteAdminByAccount(String account);

    Admin updateAdmin(int id, Admin admin);
}
