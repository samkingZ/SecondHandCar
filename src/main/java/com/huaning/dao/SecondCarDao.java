package com.huaning.dao;

import com.huaning.pojo.SecondCar;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface SecondCarDao {

    void addSecondCar(SecondCar secondCar);

    SecondCar getSecondCarById(int id);

    int getPulishedCarNum();

    List<SecondCar> getSecondCarByUserId(int id);


    List<SecondCar> getSecondCarByPriceRange(int low, int high);

    List<SecondCar> getLatestPublishedCar(int beginIndex, int endIndex);

    List<SecondCar> getSecondCarByCarId(int id);

    List<SecondCar> getSecondCarByMileageRange(int low, int high);

    void deleteSecondCarById(int id);

    void deleteSecondCarByUserId(int id);

    void deleteSecondCarByCarId(int id);

    SecondCar updateSecondCar(int id, SecondCar secondCar);

}
