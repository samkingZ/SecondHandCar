package com.huaning.dao;

import com.huaning.pojo.News;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface NewsDao {
    void addNews(News news);

    News getNewsById(int id);

    List<News> getNewsByKeyword(String keyword);

    List<News> getMostViewdNews();

    List<News> getLatestNews();

    void deleteNewsById(int id);

    News updateNews(int id, News news);
}
