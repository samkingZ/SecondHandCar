package com.huaning.dao;

import com.huaning.pojo.User;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface UserDao {
    void addUser(User user);

    User getUserById(int id);

    User getUserByAccount(String account);

    void deleteUserById(int id);

    void deleteUserByAccount(String account);

    User updateUserById(int id, User user);

    User updateUserByAccount(String account, User user);

    List<User> getUsersBySql(String sql);

}
