package com.huaning.dao;

import com.huaning.pojo.CarQuestion;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface CarQuestionDao {
    void addQuestion(CarQuestion question);

    CarQuestion getQuestionById(int id);

    CarQuestion getQuestionByKeyword(String keyword);

    List<CarQuestion> getMostPopularQuestion(int num);

    List<CarQuestion> getLatestQuestion(int num);

    void deleteCarQuestionById(int id);

    CarQuestion updateCarQuestion(int id, CarQuestion question);
}
