package com.huaning.dao;

import com.huaning.pojo.CarModel;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface CarModelDao {
    void addCarModel(CarModel carModel);

    CarModel getCarModelById(int id);

    CarModel getCarModelByName(String name);

    List<CarModel> getCarModelsByBrandId(int id);

    void deleteCarModelById(int id);

    void deleteCarModelByName(String name);

    void deleteCarModelByBrandId(int id);

    CarModel updateCarModel(int id, CarModel carModel);

}
