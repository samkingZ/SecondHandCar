package com.huaning.dao;

import com.huaning.pojo.CarBrand;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface CarBrandDao {
    void addCarBrand(CarBrand brand);

    CarBrand getCarBrandById(int id);

    CarBrand getCarBrandByName(String name);

    void deleteCarBrandById(int id);

    void deleteCarBrandByName(String name);

    CarBrand updateCarBrand(int id, CarBrand carBrand);

    List<CarBrand> getCarBrandsBySql(String sql);

    List<CarBrand> getAllCarBrands();
}
