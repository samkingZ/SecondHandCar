package com.huaning.dao;

import com.huaning.pojo.Car;

import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public interface CarDao {
    void addCar(Car car);

    Car getCarById(int id);

    Car getCarByName(String name);

    List<Car> getCarByCarModelId(int id);

    List<Car> getCarByCarBrandId(int id);

    List<Car> getCarBySql(String sql);

    void deleteCarById(int id);

    void deleteCarByModelId(int id);

    Car updateCar(int id, Car car);

}
