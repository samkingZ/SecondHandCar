package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.CarAnswerDao;
import com.huaning.pojo.CarAnswer;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarAnswerDaoImpl extends BaseDao implements CarAnswerDao{

    private static Logger logger = Logger.getLogger(CarAnswerDao.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<CarAnswer> list = new ArrayList<CarAnswer>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new CarAnswer(
                            result.getInt("id"), result.getInt("carQuestionID"),
                            result.getString("answer"), result.getString("createTime"),
                            result.getInt("zanNum")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    @Override
    public void addCarAnswer(CarAnswer carAnswer) {
        String sql = "INSERT INTO caranswer(carQuestionID, answer, createTime, zanNum)" +
                " VALUES(?,?,?,?)";
        Object[] args = {
                carAnswer.getCarQuestionID(),
                carAnswer.getAnswer(),
                carAnswer.getCreateTime(),
                carAnswer.getZanNum()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarAnswer getCarAnswerById(int id) {
        String sql = "SELECT * FROM caranswer WHERE id=?";
        Object[] args = {
                id
        };
        List<CarAnswer> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public List<CarAnswer> getCarAnswersByQuestionId(int id) {
        String sql = "SELECT * FROM caranswer WHERE carQuestionID=?";
        Object[] args = {
                id
        };
        List<CarAnswer> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public void deleteCarAnswerById(int id) {
        String sql = "DELETE FROM caranswer where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteCarAnswerByQuestionId(int id) {
        String sql = "DELETE FROM caranswer where carQuestionID = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarAnswer updateCarAnswer(int id, CarAnswer carAnswer) {
        String sql = "UPDATE caranswer SET carQuestionID=?, answer=?, createTime=?," +
                "zanNum=? WHERE id=?";
        Object[] args = {
                carAnswer.getCarQuestionID(),
                carAnswer.getAnswer(),
                carAnswer.getCreateTime(),
                carAnswer.getZanNum(),
                id
        };
        excuteUpdate(sql, args);
        return getCarAnswerById(id);
    }
}
