package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.CarQuestionDao;
import com.huaning.pojo.CarQuestion;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarQuestionDaoImpl extends BaseDao implements CarQuestionDao {

    private static Logger logger = Logger.getLogger(CarQuestionDaoImpl.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<CarQuestion> list = new ArrayList<CarQuestion>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new CarQuestion(
                            result.getInt("id"), result.getString("question"),
                            result.getString("createTime"), result.getInt("pageView")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    @Override
    public CarQuestion updateCarQuestion(int id, CarQuestion question) {
        String sql = "UPDATE carquestion SET question=?, createTime=?, pageView=? WHERE id=?";
        Object[] args = {
                question.getQuestion(),
                question.getCreateTime(),
                question.getPageView(),
                id
        };
        excuteUpdate(sql, args);
        return getQuestionById(id);
    }

    @Override
    public void deleteCarQuestionById(int id) {
        String sql = "DELETE FROM carquestion where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public List<CarQuestion> getLatestQuestion(int num) {
        String sql = "SELECT * FROM carquestion ORDER BY createTime DESC LIMIT ? ";
        Object[] args = {
                num
        };
        List<CarQuestion> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<CarQuestion> getMostPopularQuestion(int num) {
        String sql = "SELECT * FROM carquestion ORDER BY pageView DESC LIMIT ? ";
        Object[] args = {
                num
        };
        List<CarQuestion> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public CarQuestion getQuestionByKeyword(String keyword) {
        String sql = "SELECT * FROM carquestion WHERE question like ?";
        Object[] args = {
                "%"+keyword+"%"
        };
        List<CarQuestion> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public void addQuestion(CarQuestion question) {
        String sql = "INSERT INTO carquestion(question, createTime, pageView)" +
                " VALUES(?, ?, ?)";
        Object[] args = {
                question.getQuestion(),
                question.getCreateTime(),
                question.getPageView()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarQuestion getQuestionById(int id) {
        String sql = "SELECT * FROM carquestion WHERE id=?";
        Object[] args = {
                id
        };
        List<CarQuestion> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }
}
