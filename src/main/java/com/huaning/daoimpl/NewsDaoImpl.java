package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.NewsDao;
import com.huaning.pojo.News;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public class NewsDaoImpl extends BaseDao implements NewsDao {

    private static Logger logger = Logger.getLogger(NewsDaoImpl.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<News> list = new ArrayList<News>();
        if(result!=null){
            try {
                while(result.next()){
                    list.add(new News(
                            result.getInt("id"),
                            result.getString("title"),
                            result.getString("content"),
                            result.getString("source"),
                            result.getString("date"),
                            result.getInt("pageView")
                    ));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(this.getClass().getName() + "changeResultToList 出现错误");
            }
        }
        return list;
    }

    @Override
    public void addNews(News news) {
        String sql = "INSERT INTO News(title, content, source, date, pageView)" +
                " VALUES(?,?,?,?,?)";
        Object[] args = {
                news.getTitle(), news.getContent(), news.getSource(),
                news.getDate(), news.getPageView()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public News getNewsById(int id) {
        String sql = "SELECT * FROM news WHERE id=?";
        Object[] args = {
                id
        };
        List<News> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public List<News> getNewsByKeyword(String keyword) {
        String sql = "SELECT * FROM news WHERE title like ?";
        Object[] args = {
                "%"+keyword+"%"
        };
        List<News> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<News> getMostViewdNews() {
        String sql = "SELECT * FROM news order by pageView desc";
        List<News> list = executeQuery(sql);
        return list;
    }

    @Override
    public List<News> getLatestNews() {
        String sql = "SELECT * FROM news order by date desc";
        List<News> list = executeQuery(sql);
        return list;
    }

    @Override
    public void deleteNewsById(int id) {
        String sql = "DELETE FROM news WHERE id=?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public News updateNews(int id, News news) {
        String sql = "UPDATE user SET title=?, content=?, source=?," +
                "date=?, pageView=? where id=?";
        Object[] args = {
                news.getTitle(),
                news.getContent(),
                news.getSource(),
                news.getDate(),
                news.getPageView(),
                id
        };
        excuteUpdate(sql, args);
        return getNewsById(id);
    }
}
