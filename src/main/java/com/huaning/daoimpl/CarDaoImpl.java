package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.CarDao;
import com.huaning.pojo.Car;
import com.huaning.pojo.CarModel;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarDaoImpl extends BaseDao implements CarDao {

    private static Logger logger = Logger.getLogger(CarDaoImpl.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<Car> list = new ArrayList<Car>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new Car(
                            result.getInt("id"), result.getInt("carModelId"),
                            result.getString("name"), result.getInt("horsepower"),
                            result.getInt("displacement"), result.getInt("length"),
                            result.getInt("width"), result.getInt("height"),
                            result.getInt(("weight")),result.getInt("seatcount"),
                            result.getInt("price"), result.getString("model"),
                            result.getString(("gearbox")),result.getString(("description"))
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    @Override
    public void addCar(Car car) {
        String sql = "INSERT INTO car(carModelId, name, horsepower, displacement, " +
                "length, width,height,weight,seatcount, price, model, gearbox, description)" +
                " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args = {
                car.getCarModelId(),
                car.getName(),
                car.getHorsepower(),
                car.getDisplacement(),
                car.getLength(),
                car.getWidth(),
                car.getHeight(),
                car.getWeight(),
                car.getSeatcount(),
                car.getPrice(),
                car.getModel(),
                car.getGearbox(),
                car.getDescription()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public Car getCarById(int id) {
        String sql = "SELECT * FROM car WHERE id=?";
        Object[] args = {
                id
        };
        List<Car> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public Car getCarByName(String name) {
        String sql = "SELECT * FROM car WHERE name=?";
        Object[] args = {
                name
        };
        List<Car> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public List<Car> getCarByCarModelId(int id) {
        String sql = "SELECT * FROM car WHERE carModelId=?";
        Object[] args = {
                id
        };
        List<Car> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<Car> getCarByCarBrandId(int id) {
        List<CarModel> modelList = new CarModelDaoImpl().getCarModelsByBrandId(id);
        List<Car> carList = new ArrayList<Car>();
        for(CarModel model : modelList){
            carList.addAll(getCarByCarModelId(model.getId()));
        }
        return carList;
    }

    @Override
    public List<Car> getCarBySql(String sql) {
        List<Car> list = executeQuery(sql);
        return list;
    }

    @Override
    public void deleteCarById(int id) {
        String sql = "DELETE FROM car where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteCarByModelId(int id) {
        String sql = "DELETE FROM car where carModelId = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public Car updateCar(int id, Car car) {
        String sql = "UPDATE car SET carModelId=?, name=?, horsepower=?, displacement=?," +
                "length=?, width=?,height=?,weight=?,seatcount=?, price=?, model=?, gearbox=?, description=?" +
                " WHERE id=?";
        Object[] args = {
                car.getCarModelId(),
                car.getName(),
                car.getHorsepower(),
                car.getDisplacement(),
                car.getLength(),
                car.getWidth(),
                car.getHeight(),
                car.getWeight(),
                car.getSeatcount(),
                car.getPrice(),
                car.getModel(),
                car.getGearbox(),
                car.getDescription(),
                id
        };
        excuteUpdate(sql, args);
        return getCarById(id);
    }
}
