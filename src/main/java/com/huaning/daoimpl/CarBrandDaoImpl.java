package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.CarBrandDao;
import com.huaning.pojo.CarBrand;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarBrandDaoImpl extends BaseDao implements CarBrandDao {

    private static Logger logger = Logger.getLogger(CarBrandDaoImpl.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<CarBrand> list = new ArrayList<CarBrand>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new CarBrand(
                            result.getInt("id"), result.getString("name"),
                            result.getString("country"), result.getString("description")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    @Override
    public void addCarBrand(CarBrand brand) {
        String sql = "INSERT INTO carbrand(name, country, description)" +
                " VALUES(?,?,?)";
        Object[] args = {
                brand.getName(),
                brand.getCountry(),
                brand.getDescription()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarBrand getCarBrandById(int id) {
        String sql = "SELECT * FROM carbrand WHERE id=?";
        Object[] args = {
                id
        };
        List<CarBrand> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public CarBrand getCarBrandByName(String name) {
        String sql = "SELECT * FROM carbrand WHERE name=?";
        Object[] args = {
                name
        };
        List<CarBrand> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public void deleteCarBrandById(int id) {
        String sql = "DELETE FROM carbrand where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteCarBrandByName(String name) {
        String sql = "DELETE FROM carbrand where name= ?";
        Object[] args = {
                name
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarBrand updateCarBrand(int id, CarBrand carBrand) {
        String sql = "UPDATE carbrand SET name=?, country=?, description=?," +
                "WHERE id=?";
        Object[] args = {
                carBrand.getName(),
                carBrand.getCountry(),
                carBrand.getDescription(),
                id
        };
        excuteUpdate(sql, args);
        return getCarBrandById(id);
    }

    @Override
    public List<CarBrand> getCarBrandsBySql(String sql) {
        List<CarBrand> list = executeQuery(sql, null);
        return list;
    }

    @Override
    public List<CarBrand> getAllCarBrands() {
        String sql = "SELECT * FROM carbrand ORDER BY name";
        List<CarBrand> list = executeQuery(sql);
        return list;
    }
}
