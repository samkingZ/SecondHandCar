package com.huaning.daoimpl;

import com.huaning.dao.AdminDao;
import com.huaning.dao.BaseDao;
import com.huaning.pojo.Admin;
import com.huaning.util.JdbcUtil;
import jdk.nashorn.internal.scripts.JD;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public class AdminDaoImpl extends BaseDao implements AdminDao{

    private static Logger logger = Logger.getLogger(AdminDaoImpl.class);

    @Override
    protected List<Admin> changeResultTOList(ResultSet result){
        List<Admin> list = new ArrayList<Admin>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new Admin(
                            result.getInt("id"), result.getString("account"),
                            result.getString("password"), result.getString("name"),
                            result.getString("telphone"), result.getString("mail"),
                            result.getString("latestLogin")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    public void addAdmin(Admin admin) {
        String sql = "INSERT INTO admin(account, password, name, telphone, mail, latestLogin)" +
                " VALUES(?,?,?,?,?,?)";
        Object[] args = {
                admin.getAccount(), admin.getPassword(), admin.getName(),
                admin.getTelphone(), admin.getMail(), admin.getlatestLogin()
        };
        excuteUpdate(sql, args);
    }

    public Admin getAdminById(int id) {
        String sql = "SELECT * FROM admin WHERE id=?";
        Object[] args = {
                id
        };
        List<Admin> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    public Admin getAdminByAccount(String account) {
        String sql = "SELECT * FROM Admin WHERE account=?";
        Object[] args = {
                account
        };
        List<Admin> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    public void deleteAdminById(int id) {
        String sql = "DELETE FROM admin where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    public void deleteAdminByAccount(String account) {
        String sql = "DELETE FROM admin where account = ?";
        Object[] args = {
                account
        };
        excuteUpdate(sql, args);
    }

    public Admin updateAdmin(int id, Admin admin) {
        String sql = "UPDATE admin SET account=?, password=?, name=?," +
                "telphone=?, mail=?, latestLogin=? WHERE id=?";
        Object[] args = {
                admin.getAccount(),
                admin.getPassword(),
                admin.getName(),
                admin.getTelphone(),
                admin.getMail(),
                admin.getlatestLogin(),
                id
        };
        excuteUpdate(sql, args);
        return getAdminById(id);
    }
}
