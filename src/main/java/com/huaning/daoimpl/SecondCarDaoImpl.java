package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.SecondCarDao;
import com.huaning.pojo.SecondCar;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class SecondCarDaoImpl extends BaseDao implements SecondCarDao {

    private static Logger logger = Logger.getLogger(SecondCarDaoImpl.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<SecondCar> list = new ArrayList<SecondCar>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new SecondCar(
                            result.getInt("id"), result.getInt("carId"),
                            result.getInt("userId"), result.getInt("price"),
                            result.getString("getLicenseDate"), result.getString("getLicensePlace"),
                            result.getInt("mileage"),result.getString("emissionStandard"),
                            result.getString("publishDate")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    @Override
    public void addSecondCar(SecondCar secondCar) {
        String sql = "INSERT INTO secondcar(carId, userId, price, getLicenseDate, getLicensePlace,mileage,emissionStandard,publishDate)" +
                " VALUES(?,?,?,?,?,?,?,?)";
        Object[] args = {
                secondCar.getCarId(),
                secondCar.getUserId(),
                secondCar.getPrice(),
                secondCar.getGetLicenseDate(),
                secondCar.getGetLicensePlace(),
                secondCar.getMileage(),
                secondCar.getEmissionStandard(),
                secondCar.getPublishDate()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public SecondCar getSecondCarById(int id) {
        String sql = "SELECT * FROM SecondCar WHERE id=?";
        Object[] args = {
                id
        };
        List<SecondCar> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    public int getPulishedCarNum(){
        String sql = "SELECT * FROM SecondCar";
        List<SecondCar> list = executeQuery(sql);
        return list.size();
    }

    @Override
    public List<SecondCar> getSecondCarByUserId(int id) {
        String sql = "SELECT * FROM SecondCar WHERE userId=?";
        Object[] args = {
                id
        };
        List<SecondCar> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<SecondCar> getSecondCarByPriceRange(int low, int high) {
        String sql = "SELECT * FROM SecondCar WHERE price BETWEEN ? AND ?";
        Object[] args = {
                low,
                high
        };
        List<SecondCar> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<SecondCar> getSecondCarByMileageRange(int low, int high) {
        String sql = "SELECT * FROM SecondCar WHERE mileage BETWEEN ? AND ?";
        Object[] args = {
                low,
                high
        };
        List<SecondCar> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<SecondCar> getLatestPublishedCar(int beginIndex, int endIndex) {
        String sql = "SELECT * FROM SecondCar ORDER BY publishDate DESC LIMIT ?,?";
        Object[] args = {
                beginIndex,
                endIndex-beginIndex
        };
        List<SecondCar> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public List<SecondCar> getSecondCarByCarId(int id) {
        String sql = "SELECT * FROM SecondCar WHERE carId=?";
        Object[] args = {
                id
        };
        List<SecondCar> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public void deleteSecondCarById(int id) {
        String sql = "DELETE FROM SecondCar where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteSecondCarByUserId(int id) {
        String sql = "DELETE FROM SecondCar where userId= ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteSecondCarByCarId(int id) {
        String sql = "DELETE FROM SecondCar where carId= ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public SecondCar updateSecondCar(int id, SecondCar secondCar) {
        String sql = "UPDATE admin SET carid=?, userId=?, price=?," +
                "getLicenseDate=?, getLicensePlace=?, mileage=?, " +
                "emissionStandard=?, publishDate=? WHERE id=?";
        Object[] args = {
                secondCar.getCarId(),
                secondCar.getUserId(),
                secondCar.getPrice(),
                secondCar.getGetLicenseDate(),
                secondCar.getGetLicensePlace(),
                secondCar.getMileage(),
                secondCar.getEmissionStandard(),
                secondCar.getPublishDate(),
                id
        };
        excuteUpdate(sql, args);
        return getSecondCarById(id);
    }
}
