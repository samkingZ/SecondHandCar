package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.CarModelDao;
import com.huaning.pojo.CarModel;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/13/16.
 */
public class CarModelDaoImpl extends BaseDao implements CarModelDao{

    private static Logger logger = Logger.getLogger(CarModelDaoImpl.class);

    @Override
    protected List changeResultTOList(ResultSet result) {
        List<CarModel> list = new ArrayList<CarModel>();
        try {
            if(result!=null) {
                while (result.next()) {
                    list.add(new CarModel(
                            result.getInt("id"), result.getInt("carBrandId"),
                            result.getString("name"), result.getString("description")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(this.getClass().getName() + "changeResultToList 出现错误");
        }
        return list;
    }

    @Override
    public void addCarModel(CarModel carModel) {
        String sql = "INSERT INTO carmodel(carBrandId, name, description)" +
                " VALUES(?,?,?)";
        Object[] args = {
                carModel.getCarBrandId(),
                carModel.getName(),
                carModel.getDescription()
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarModel getCarModelById(int id) {
        String sql = "SELECT * FROM carmodel WHERE id=?";
        Object[] args = {
                id
        };
        List<CarModel> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public CarModel getCarModelByName(String name) {
        String sql = "SELECT * FROM carmodel WHERE name=?";
        Object[] args = {
                name
        };
        List<CarModel> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    @Override
    public List<CarModel> getCarModelsByBrandId(int id) {
        String sql = "SELECT * FROM carmodel WHERE carBrandId=?";
        Object[] args = {
                id
        };
        List<CarModel> list = executeQuery(sql, args);
        return list;
    }

    @Override
    public void deleteCarModelById(int id) {
        String sql = "DELETE FROM carmodel where id = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteCarModelByName(String name) {
        String sql = "DELETE FROM carmodel where name = ?";
        Object[] args = {
                name
        };
        excuteUpdate(sql, args);
    }

    @Override
    public void deleteCarModelByBrandId(int id) {
        String sql = "DELETE FROM carmodel where carBrandId = ?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    @Override
    public CarModel updateCarModel(int id, CarModel carModel) {
        String sql = "UPDATE carmodel SET carBrandId=?, name=?, description=?," +
                "WHERE id=?";
        Object[] args = {
                carModel.getCarBrandId(),
                carModel.getName(),
                carModel.getDescription(),
                id
        };
        excuteUpdate(sql, args);
        return getCarModelById(id);
    }
}
