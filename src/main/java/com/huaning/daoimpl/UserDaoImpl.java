package com.huaning.daoimpl;

import com.huaning.dao.BaseDao;
import com.huaning.dao.UserDao;
import com.huaning.pojo.User;
import com.huaning.util.JdbcUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by disneyland on 7/12/16.
 */
public class UserDaoImpl extends BaseDao implements UserDao{

    private static Logger logger = Logger.getLogger(UserDaoImpl.class);

    @Override
    protected List<User> changeResultTOList(ResultSet result){
        List<User> list = new ArrayList<User>();
        if(result!=null){
            try {
                while(result.next()){
                    list.add(new User(
                            result.getInt("id"),
                            result.getString("account"),
                            result.getString("password"),
                            result.getString("name"),
                            result.getString("telphone"),
                            result.getString("mail")
                            ));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(this.getClass().getName() + "changeResultToList 出现错误");
            }
        }
        return list;
    }

    public void addUser(User user) {
        String sql = "INSERT INTO User(account, password, name, telphone, mail)" +
                " VALUES(?,?,?,?,?)";
        Object[] args = {
                user.getAccount(), user.getPassword(), user.getName(),
                user.getTelphne(), user.getMail()
        };
        excuteUpdate(sql, args);
    }

    public User getUserById(int id) {
        String sql = "SELECT * FROM user WHERE id=?";
        Object[] args = {
                id
        };
        List<User> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    public User getUserByAccount(String account) {
        String sql = "SELECT * FROM user WHERE account=?";
        Object[] args = {
                account
        };
        List<User> list = executeQuery(sql, args);
        return list.size()>0? list.get(0) : null;
    }

    public void deleteUserById(int id) {
        String sql = "DELETE FROM user WHERE id=?";
        Object[] args = {
                id
        };
        excuteUpdate(sql, args);
    }

    public void deleteUserByAccount(String account) {
        String sql = "DELETE FROM user WHERE account=?";
        Object[] args = {
                account
        };
        excuteUpdate(sql, args);
    }

    public User updateUserById(int id, User user) {
        String sql = "UPDATE user SET account=?, password=?, name=?," +
                "telphone=?, mail=? where id=?";
        Object[] args = {
                user.getAccount(),
                user.getPassword(),
                user.getName(),
                user.getTelphne(),
                user.getMail(),
                id
        };
        excuteUpdate(sql, args);
        return getUserById(id);
    }

    public User updateUserByAccount(String account, User user) {
        String sql = "UPDATE user SET account=?, password=?, name=?," +
                "telphone=?, mail=? where account=?";
        Object[] args = {
                user.getAccount(),
                user.getPassword(),
                user.getName(),
                user.getTelphne(),
                user.getMail(),
                account
        };
        excuteUpdate(sql, args);
        return getUserByAccount(account);
    }

    public List<User> getUsersBySql(String sql) {
        List<User> list = executeQuery(sql);
        return list;
    }
}
