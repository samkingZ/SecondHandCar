package com.huaning.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by disneyland on 7/13/16.
 */
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)resp;
        HttpSession session = request.getSession();
        String path = request.getRequestURI();
        String userAccount = (String)session.getAttribute("userAccount");
        String adminAccount = (String)session.getAttribute("adminAccount");
        if(userAccount == null || "".equals(userAccount) && adminAccount == null || "".equals(adminAccount)){
            request.getRequestDispatcher("/login").forward(request,response);
        }
        else{
            chain.doFilter(req, resp);
        }

    }

    public void init(FilterConfig config) throws ServletException {

    }

}
