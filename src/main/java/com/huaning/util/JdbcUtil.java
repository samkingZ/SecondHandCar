package com.huaning.util;

import javax.sql.DataSource;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.log4j.Logger;

/**
 * Created by disneyland on 7/11/16.
 */
public class JdbcUtil {
    private static DataSource ds = null;
    private static Logger logger = Logger.getLogger(JdbcUtil.class);
    static{
        try{
            InputStream in = JdbcUtil.class.getClassLoader().getResourceAsStream("dbcpconfig.properties");
            Properties prop = new Properties();
            prop.load(in);
            ds = BasicDataSourceFactory.createDataSource(prop);
            System.out.println("获取数据原");
        }
        catch (Exception e){
            e.printStackTrace();
            logger.error("获取数据源失败");
        }
        //System.out.println("连接数据库正确");
    }

    public static Connection getConnection(){
        Connection conn = null;
        try {
           conn = ds.getConnection();
        } catch (SQLException e) {
            logger.warn("获取连接失败");
        }
        return conn;
    }

    public static void releaseConnection(Connection conn){
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                logger.warn("释放连接失败");
            }
        }
    }

    public static void releaseResultSet(ResultSet result){
        if(result != null){
            try {
                result.close();
            } catch (SQLException e) {
                logger.warn("释放结果集失败");
            }
        }
    }

    public static void releaseStatement(Statement stat){
        if(stat != null){
            try {
                stat.close();
            } catch (SQLException e) {
                logger.warn("释放Statement失败");
            }
        }
    }

    public static PreparedStatement getPreparedStatement(Connection conn,String sql){
        PreparedStatement preStat = null;
        try {
            preStat = conn.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("获取PrepareStatement失败");
        }
        return preStat;
    }

    private static void fillPreStatArgs(PreparedStatement preStat, Object[] args){
        if (args == null)
            return ;
        for(int i=1; i<args.length+1; i++){
            try {
                if(args[i-1] instanceof String){
                    preStat.setString(i, (String)args[i-1]);
                }
                else if(args[i-1] instanceof Integer){
                    preStat.setInt(i, (Integer) args[i-1]);
                }
                else if(args[i-1] instanceof Long){
                    preStat.setLong(i, (Long) args[i-1]);
                }
                else if(args[i-1] instanceof Double){
                    preStat.setDouble(i, (Double)args[i-1]);
                }
                else{
                    preStat.setObject(i, args[i-1]);
                }
            } catch (SQLException e) {
                logger.error("PreparedStatement设置参数出错");
            }
        }
    }

    public static void excuteUpdate(PreparedStatement preStat, Object[] args){

        fillPreStatArgs(preStat, args);
        try {
            preStat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("JdbcUtil Prestatement executeUpdate 失败");
        }
    }

    public static ResultSet executeQuery(PreparedStatement preStat, Object[] args){
        fillPreStatArgs(preStat, args);
        ResultSet result = null;
        try{
            result = preStat.executeQuery();
        }catch (SQLException e){
            e.printStackTrace();
            logger.error("JdbcUtil Prestatement executeUpdate 失败");
        }
        return result;
    }

}
