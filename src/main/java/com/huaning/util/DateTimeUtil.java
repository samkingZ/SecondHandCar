package com.huaning.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by disneyland on 7/12/16.
 */
public class DateTimeUtil {
    private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue(){
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
        }
    };

    public static String getSqlDateTime(Date date){
        return threadLocal.get().format(date);
    }
}
