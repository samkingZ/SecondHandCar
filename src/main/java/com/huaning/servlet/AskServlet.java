package com.huaning.servlet;

import com.huaning.serviceimpl.CarQuestionServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.huaning.service.CarQuestionService;

/**
 * Created by disneyland on 7/13/16.
 */

public class AskServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CarQuestionService carQuestionService = new CarQuestionServiceImpl();
        request.setAttribute("popularQuestion",  carQuestionService.getMostPopularQuestion(10));
        request.setAttribute("popularQuestion",  carQuestionService.getMostPopularQuestion(10));
        RequestDispatcher rd = request.getRequestDispatcher("/jsp/ask.jsp");
        rd.forward(request, response);
    }
}
