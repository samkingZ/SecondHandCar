package com.huaning.servlet;

import com.huaning.dao.SecondCarDao;
import com.huaning.daoimpl.SecondCarDaoImpl;
import com.huaning.service.SecondCarService;
import com.huaning.serviceimpl.SecondCarServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by disneyland on 7/13/16.
 */

public class MainPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       // SecondCarService secondCarService = new SecondCarServiceImpl();
        //request.setAttribute("carList", secondCarService.getLatestPublishedCar(12));
        RequestDispatcher rd = request.getRequestDispatcher("/jsp/home.jsp");
        rd.forward(request, response);
    }
}
