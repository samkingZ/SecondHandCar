package com.huaning.servlet;

import com.huaning.service.UserService;
import com.huaning.serviceimpl.AdminServiceImpl;
import com.huaning.serviceimpl.UserServiceImpl;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by disneyland on 7/11/16.
 */

public class LoginServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String dispatcherPage = "/jsp/error.jsp";
        if(new UserServiceImpl().login(account, password)){
            HttpSession session = request.getSession();
            session.setAttribute("userAccount", account);
            dispatcherPage = "/jsp/userHome.jsp";
        }
        else if( new AdminServiceImpl().checkLogin(account, password)){
            HttpSession session = request.getSession();
            session.setAttribute("adminAccount", account);
            dispatcherPage = "/jsp/adminHome.jsp";
        }
        System.out.println(dispatcherPage);
        request.setAttribute("error", "用户名或密码错误");
        request.getRequestDispatcher(dispatcherPage).forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/jsp/login.jsp");
        rd.forward(request, response);
    }

}
