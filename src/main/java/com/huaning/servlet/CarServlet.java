package com.huaning.servlet;

import com.huaning.dao.SecondCarDao;
import com.huaning.pojo.Car;
import com.huaning.pojo.SecondCar;
import com.huaning.pojo.User;
import com.huaning.service.CarService;
import com.huaning.service.SecondCarService;
import com.huaning.service.UserService;
import com.huaning.serviceimpl.CarServiceImpl;
import com.huaning.serviceimpl.SecondCarServiceImpl;
import com.huaning.serviceimpl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Created by disneyland on 7/13/16.
 */

public class CarServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String uri = request.getRequestURI();
        if(uri.endsWith("sellcar")){
            postSellCar(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if(uri.endsWith("buycar")){
            buyCar(request, response);
        }
        else if(uri.endsWith("sellcar")){
            getSellCar(request, response);
        }
        else{
            detail(request, response);
        }
    }

    private void buyCar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SecondCarService secondCarService = new SecondCarServiceImpl();
        int page = 0;
        if(request.getParameter("page") != null){
            page = Integer.valueOf(request.getParameter("page"));
        }
        int pageSize = 12;
        int pageNum = secondCarService.getPulishedCarNum()/pageSize + 1;
        List<SecondCar> secondCarList = secondCarService.getLatestPublishedCarByPage(page*pageSize, pageSize);
        request.setAttribute("pageNum", pageNum);
        request.setAttribute("secondCarList", secondCarList);
        request.getRequestDispatcher("./jsp/buycar.jsp").forward(request, response);
    }

    private void getSellCar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("./jsp/sellcar.jsp").forward(request, response);
    }

    private  void postSellCar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userAccount = (String)request.getSession().getAttribute("userAccount");
        UserService userService = new UserServiceImpl();
        SecondCarService secondCarService = new SecondCarServiceImpl();
        User user = userService.getUserByAccount(userAccount);
        SecondCar secondCar = new SecondCar(
                Math.abs(new Random().nextInt()%7+13),
                user.getId(),
                Integer.valueOf(request.getParameter("price")),
                request.getParameter("getLicenseDate"),
                request.getParameter("getLicensePlace"),
                Integer.valueOf(request.getParameter("mileage")),
                request.getParameter("emissionStandard")
        );
        System.out.println("sellcar");
        secondCarService.addSecondCar(secondCar);
        request.getRequestDispatcher("./jsp/userhome.jsp").forward(request, response);
    }

    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SecondCarService secondCarService = new SecondCarServiceImpl();
        String str = request.getParameter("id");
        int secondCarId = Integer.valueOf(request.getParameter("id")) ;
        //String carBrand = request.getParameter("brand");
        //String carModel = request.getParameter("model");
        SecondCar secondCar = secondCarService.getSecondCarBySecondCarId(secondCarId);
        request.setAttribute("secondCar", secondCar);
        request.getRequestDispatcher("./jsp/detail.jsp").forward(request, response);
    }
}
