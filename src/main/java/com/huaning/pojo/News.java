package com.huaning.pojo;

import com.huaning.util.DateTimeUtil;

import java.util.Date;


/**
 * Created by disneyland on 7/12/16.
 */
public class News {
    private int id;
    private String title;
    private String content;
    private String source;
    private String date;
    private int pageView;

    public News() {
    }

    public News(String title, String content, String source) {
        this.title = title;
        this.content = content;
        this.source = source;
        this.date = DateTimeUtil.getSqlDateTime(new Date());
        this.pageView = 0;
    }

    public News(int id, String title, String content, String source, String date, int pageView) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.source = source;
        this.date = date;
        this.pageView = pageView;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPageView() {
        return this.pageView;
    }

    public void setPageView(int pageView) {
        this.pageView = pageView;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(Date date) {
       this.date = DateTimeUtil.getSqlDateTime(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        News news = (News) o;

        if (this.id != news.id) return false;
        if (this.title != null ? !this.title.equals(news.title) : news.title != null) return false;
        if (this.content != null ? !this.content.equals(news.content) : news.content != null) return false;
        if (this.source != null ? !this.source.equals(news.source) : news.source != null) return false;
        return this.date != null ? this.date.equals(news.date) : news.date == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + (this.title != null ? this.title.hashCode() : 0);
        result = 31 * result + (this.content != null ? this.content.hashCode() : 0);
        result = 31 * result + (this.source != null ? this.source.hashCode() : 0);
        result = 31 * result + (this.date != null ? this.date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", source='" + source + '\'' +
                ", date=" + date +
                '}';
    }


}
