package com.huaning.pojo;

import com.huaning.util.DateTimeUtil;

import java.util.Date;

/**
 * Created by disneyland on 7/12/16.
 */
public class Admin {
    private int id;
    private String account;
    private String password;
    private String name;
    private String telphone;
    private String mail;
    private String latestLogin;

    public Admin() {
    }

    public Admin(String account, String password, String name, String telphone, String mail) {
        this.account = account;
        this.password = password;
        this.name = name;
        this.telphone = telphone;
        this.mail = mail;
        this.latestLogin = DateTimeUtil.getSqlDateTime(new Date());
    }

    public Admin(int id, String account, String password, String name, String telphone, String mail, String latestLogin) {
        this.id = id;
        this.account = account;
        this.password = password;
        this.name = name;
        this.telphone = telphone;
        this.mail = mail;
        this.latestLogin = latestLogin;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelphone() {
        return this.telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getlatestLogin() {
        return this.latestLogin;
    }

    public void setlatestLogin(Date date) {
        this.latestLogin = DateTimeUtil.getSqlDateTime(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;

        if (this.id != admin.id) return false;
        if (this.account != null ? !this.account.equals(admin.account) : admin.account != null) return false;
        if (this.password != null ? !this.password.equals(admin.password) : admin.password != null) return false;
        if (this.name != null ? !this.name.equals(admin.name) : admin.name != null) return false;
        if (this.telphone != null ? !this.telphone.equals(admin.telphone) : admin.telphone != null) return false;
        return this.mail != null ? this.mail.equals(admin.mail) : admin.mail == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + (this.account != null ? this.account.hashCode() : 0);
        result = 31 * result + (this.password != null ? this.password.hashCode() : 0);
        result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
        result = 31 * result + (this.telphone != null ? this.telphone.hashCode() : 0);
        result = 31 * result + (this.mail != null ? this.mail.hashCode() : 0);
        return result;
    }


}
