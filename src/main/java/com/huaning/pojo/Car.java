package com.huaning.pojo;

/**
 * Created by disneyland on 7/12/16.
 */
public class Car {
    private int id;
    private int carModelId;
    private String name;
    private int horsepower;
    private int displacement;
    private int length;
    private int width;
    private int height;
    private int weight;
    private int seatcount;
    private int price;
    private String model;
    private String gearbox;
    private String description;



    @Override
    public String toString() {
        return "Car{" +
                "description='" + description + '\'' +
                ", gearbox='" + gearbox + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", seatcount=" + seatcount +
                ", weight=" + weight +
                ", height=" + height +
                ", width=" + width +
                ", length=" + length +
                ", displacement=" + displacement +
                ", horsepower=" + horsepower +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (this.id != car.id) return false;
        if (this.carModelId != car.carModelId) return false;
        if (this.name != null ? !this.name.equals(car.name) : car.name != null) return false;
        return this.model != null ? this.model.equals(car.model) : car.model == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + this.carModelId;
        result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
        result = 31 * result + (this.model != null ? this.model.hashCode() : 0);
        return result;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarModelId() {

        return this.carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHorsepower() {
        return this.horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getDisplacement() {
        return this.displacement;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getSeatcount() {
        return this.seatcount;
    }

    public void setSeatcount(int seatcount) {
        this.seatcount = seatcount;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getGearbox() {
        return this.gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Car(int carModelId, String name, int horsepower, int displacement, int length, int width, int height, int weight, int seatcount, int price, String model, String gearbox, String description) {

        this.carModelId = carModelId;
        this.name = name;
        this.horsepower = horsepower;
        this.displacement = displacement;
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.seatcount = seatcount;
        this.price = price;
        this.model = model;
        this.gearbox = gearbox;
        this.description = description;
    }

    public Car(int id, int carModelId, String name, int horsepower, int displacement, int length, int width, int height, int weight, int seatcount, int price, String model, String gearbox, String description) {
        this.id = id;
        this.carModelId = carModelId;
        this.name = name;
        this.horsepower = horsepower;
        this.displacement = displacement;
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.seatcount = seatcount;
        this.price = price;
        this.model = model;
        this.gearbox = gearbox;
        this.description = description;
    }

    public Car() {
    }
}
