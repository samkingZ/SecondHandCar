package com.huaning.pojo;

/**
 * Created by disneyland on 7/12/16.
 */
public class CarBrand {
    private int id;
    private String name;
    private String country;
    private String description;

    public CarBrand() {
    }

    public CarBrand(String name, String country, String description) {
        this.name = name;
        this.country = country;
        this.description = description;
    }

    public CarBrand(int id, String name, String country, String description) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.description = description;
    }

    @Override
    public String toString() {
        return "CarBrand{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        CarBrand carBrand = (CarBrand) o;

        if (this.id != carBrand.id) return false;
        if (this.name != null ? !this.name.equals(carBrand.name) : carBrand.name != null) return false;
        return this.country != null ? this.country.equals(carBrand.country) : carBrand.country == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
        result = 31 * result + (this.country != null ? this.country.hashCode() : 0);
        return result;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {

        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
