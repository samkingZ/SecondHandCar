package com.huaning.pojo;

/**
 * Created by disneyland on 7/12/16.
 */
public class CarModel {
    private int id;
    private int carBrandId;
    private String name;
    private String description;

    public CarModel() {
    }

    public CarModel(int carBrandId, String name, String description) {
        this.carBrandId = carBrandId;
        this.name = name;
        this.description = description;
    }

    public CarModel(int id, int carBrandId, String name, String description) {
        this.id = id;
        this.carBrandId = carBrandId;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarBrandId() {
        return this.carBrandId;
    }

    public void setCarBrandId(int carBrandId) {
        this.carBrandId = carBrandId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        CarModel carModel = (CarModel) o;

        if (this.id != carModel.id) return false;
        if (this.carBrandId != carModel.carBrandId) return false;
        return this.name != null ? this.name.equals(carModel.name) : carModel.name == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + this.carBrandId;
        result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "id=" + id +
                ", carBrandId=" + carBrandId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
