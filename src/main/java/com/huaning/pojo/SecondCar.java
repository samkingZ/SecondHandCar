package com.huaning.pojo;

import com.huaning.util.DateTimeUtil;

import java.util.Date;

/**
 * Created by disneyland on 7/12/16.
 */
public class SecondCar {
    private int id;
    private int carId;
    private int userId;
    private int price;
    private String getLicenseDate;
    private String getLicensePlace;
    private int mileage;
    private String emissionStandard;
    private String publishDate;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarId() {
        return this.carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPrice() {
        return this.price;
    }

    @Override
    public String toString() {
        return "SecondCar{" +
                "emissionStandard='" + emissionStandard + '\'' +
                ", mileage=" + mileage +
                ", getLicensePlace='" + getLicensePlace + '\'' +
                ", getLicenseDate='" + getLicenseDate + '\'' +
                ", price=" + price +
                ", userId=" + userId +
                ", carId=" + carId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        SecondCar secondCar = (SecondCar) o;

        if (this.id != secondCar.id) return false;
        if (this.carId != secondCar.carId) return false;
        if (this.userId != secondCar.userId) return false;
        return this.price == secondCar.price;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + this.carId;
        result = 31 * result + this.userId;
        result = 31 * result + this.price;
        return result;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getGetLicenseDate() {
        return this.getLicenseDate;
    }

    public String getPublishDate() {
        return this.publishDate;
    }

    public void setPublishDate(Date date) {
        this.publishDate = DateTimeUtil.getSqlDateTime(date);
    }

    public void setGetLicenseDate(String getLicenseDate) {
        this.getLicenseDate = getLicenseDate;
    }

    public String getGetLicensePlace() {
        return this.getLicensePlace;
    }

    public void setGetLicensePlace(String getLicensePlace) {
        this.getLicensePlace = getLicensePlace;
    }

    public int getMileage() {
        return this.mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getEmissionStandard() {
        return this.emissionStandard;
    }

    public void setEmissionStandard(String emissionStandard) {
        this.emissionStandard = emissionStandard;
    }

    public SecondCar() {
    }

    public SecondCar(int carId, int userId, int price, String getLicenseDate, String getLicensePlace, int mileage, String emissionStandard) {

        this.carId = carId;
        this.userId = userId;
        this.price = price;
        this.getLicenseDate = getLicenseDate;
        this.getLicensePlace = getLicensePlace;
        this.mileage = mileage;
        this.emissionStandard = emissionStandard;
        this.publishDate = DateTimeUtil.getSqlDateTime(new Date());
    }

    public SecondCar(int id, int carId, int userId, int price, String getLicenseDate, String getLicensePlace, int mileage, String emissionStandard, String publishDate) {
        this.id = id;
        this.carId = carId;
        this.userId = userId;
        this.price = price;
        this.getLicenseDate = getLicenseDate;
        this.getLicensePlace = getLicensePlace;
        this.mileage = mileage;
        this.emissionStandard = emissionStandard;
        this.publishDate = publishDate;
    }
}
