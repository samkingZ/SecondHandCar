package com.huaning.pojo;

import com.huaning.util.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by disneyland on 7/12/16.
 */
public class CarQuestion {
    private int id;
    private String question;
    private String createTime;
    private int pageView;

    public CarQuestion() {
    }

    public CarQuestion(String question) {
        this.question = question;
        createTime = DateTimeUtil.getSqlDateTime(new Date());
        pageView = 0;
    }

    public CarQuestion(int id, String question, String createTime, int pageView) {
        this.id = id;
        this.question = question;
        this.createTime = createTime;
        this.pageView = pageView;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date date) {
        this.createTime = DateTimeUtil.getSqlDateTime(date);
    }

    public int getPageView() {
        return this.pageView;
    }

    public void setPageView(int pageView) {
        this.pageView = pageView;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        CarQuestion that = (CarQuestion) o;

        if (this.id != that.id) return false;
        return this.question != null ? this.question.equals(that.question) : that.question == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + (this.question != null ? this.question.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CarQuestion{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", createTime='" + createTime + '\'' +
                ", pageView=" + pageView +
                '}';
    }
}
