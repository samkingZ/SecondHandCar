package com.huaning.pojo;

/**
 * Created by disneyland on 7/11/16.
 */
public class User {
    private int id;
    private String account;
    private String password;
    private String name;
    private String telphone;
    private String mail;

    public User() {
    }

    public User(String account, String password, String name, String telphone, String mail) {
        this.account = account;
        this.password = password;
        this.name = name;
        this.telphone = telphone;
        this.mail = mail;
    }

    public User(int id, String account, String password, String name, String telphone, String mail) {
        this.id = id;
        this.account = account;
        this.password = password;
        this.name = name;
        this.telphone = telphone;
        this.mail = mail;
    }

    public int getId() {
        return this.id;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelphne() {
        return this.telphone;
    }

    public void setTelphne(String telphone) {
        this.telphone = telphone;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        User user = (User) o;

        if (this.id != user.id) return false;
        if (this.account != null ? !this.account.equals(user.account) : user.account != null) return false;
        if (this.password != null ? !this.password.equals(user.password) : user.password != null) return false;
        if (this.name != null ? !this.name.equals(user.name) : user.name != null) return false;
        if (this.telphone != null ? !this.telphone.equals(user.telphone) : user.telphone != null) return false;
        return this.mail != null ? this.mail.equals(user.mail) : user.mail == null;

    }

    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + (this.account != null ? this.account.hashCode() : 0);
        result = 31 * result + (this.password != null ? this.password.hashCode() : 0);
        result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
        result = 31 * result + (this.telphone != null ? this.telphone.hashCode() : 0);
        result = 31 * result + (this.mail != null ? this.mail.hashCode() : 0);
        return result;
    }
}