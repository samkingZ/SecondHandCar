package com.huaning.pojo;

import com.huaning.util.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by disneyland on 7/12/16.
 */
public class CarAnswer {
    private int id;
    private int carQuestionID;
    private String answer;
    private String createTime;
    private int zanNum;

    public CarAnswer() {
    }

    public CarAnswer(int carQuestionID, String answer) {
        this.carQuestionID = carQuestionID;
        this.answer = answer;
        this.createTime = DateTimeUtil.getSqlDateTime(new Date());
        this.zanNum = 0;
    }

    public CarAnswer(int id, int carQuestionID, String answer, String createTime, int zanNum) {
        this.id = id;
        this.carQuestionID = carQuestionID;
        this.answer = answer;
        this.createTime = createTime;
        this.zanNum = zanNum;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarQuestionID() {
        return this.carQuestionID;
    }

    public void setCarQuestionID(int carQuestionID) {
        this.carQuestionID = carQuestionID;
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date date) {
        this.createTime = DateTimeUtil.getSqlDateTime(date);
    }

    public int getZanNum() {
        return this.zanNum;
    }

    public void setZanNum(int zanNum) {
        this.zanNum = zanNum;
    }
}
