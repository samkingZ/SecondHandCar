<%--
  Created by IntelliJ IDEA.
  User: disneyland
  Date: 7/13/16
  Time: 12:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="./uikit/css/uikit.gradient.min.css" rel="stylesheet" type="text/css"/>
    <script src="./uikit/js/jquery-2.1.1.min.js"></script>
    <script src="./uikit/js/uikit.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style  type="text/css">

        #login{
            height:50px;
            width:100%;
        }


        #photo{height:200px;
            width:100%;
            background-image:url(image/%E6%A0%A1%E9%97%A8.jpg);}

        #navigation{background-color:#000;}


        #left{
            margin-top:2px;
            width:68%;
            float:left;

        }


        #right{
            margin-top:2px;
            width:30%;
            float:right;


        }


        #div1{
            /*      position:absolute; */
            float:left;
            margin-left:10px;
            width:10%;
        }



        #div2{

            float:left;
            width:78%;}


        #div3{  float:left; margin-right:10px;
            width:10%;}



        #div4{height:50px;
            width:100%;
            background-color:#FFF;}


        #div5{height:50px;
            background-color:#FFF;
            width:100%;}



        #sousuo{
            height:40px;
            text-align:left;
            background-color:#06F;
            color:#FFF;
        }


        #sousuo1{
            height:40px;
            padding:3px;
            width:95%;
            background-color:#FFF;}


        #suo{
            height:100px;
            width:99%;
            border-color:#000;
            border-left:1px solid #999;
            border-right:1px solid #999;
            border-bottom:1px solid #999;
        }

        #lianjie{ height:500px;
            width:100%;
            border-color:#000;
            margin-top:10px;
            color:#FFF;
            border-left:1px solid #999;
            border-right:1px solid #999;
            border-bottom:1px solid #999;
            border-top:1px solid #999;}


        #lianjie1{
            height:40px;
            padding:1px;
            width:99%;
            background-color:#06F;
        }


        #leftright{  width:100%;

            margin-top:2px;
        }

        #copyright{
            text-align:center;
            height:40px;
            width:100%;
            margin-bottom:2px;
        }


        #xiaomen{ height:200px;
            width:100%;
            background-image:url(%E6%A0%A1%E9%97%A8.jpg);
        }


        .uk-nav  ul  { display:none;}
        .uk-nav  :hover  ul {display:block;}
        .uk-nav   ul  li { float:right;
            width:30px; }
        .uk-nav{ width:150px; height:40px;}

    </style>
    <title>新闻</title>
</head>

<body style="overflow-y:scroll">
<div id="div1"><div id="div4"></div><hr /></div>
<div id="div2">
    <div  id="login">  <div  style="float:right">         <div style="clear:both;"></div>

        <a href="./login" class="uk-button" type="button" style="background-color:#0080FF; margin-top:10px; color:#999;">登陆</a>
        <a href="./register" class="uk-button" type="button" style="margin-right:10px; margin-top:10px; background-color:#0080FF; color:#999;" >注册</a>
    </div>
    </div>
    <div id="photo">湖工二手车市场</div>
    <div id="navigation">
        <nav class="uk-navbar" style="background-color:#1166AC; color:#CCC;">
            <ul class="uk-navbar-nav" >
                <li ><a href="./home" target="_self">首页</a></li>
                <li><a href="./buycar">我要买车</a></li>
                <li class="uk-parent"><a href="./sellcar">我要卖车</a></li>
                <li><a href="./news">新闻</a></li>
                <li><a href="./ask">二手车问答</a></li>
            </ul>
        </nav>
    </div>
    <div id="leftright" >
        <h2   style="margin-top:10px">咨询行情</h2><hr />
        <!-- 左 -->
        <div  id="left">
            <div>
                <p><a href="资讯1.html">奥迪A6L 特惠礼遇季 周末上映 </a></p>
                <p>来源：  2016/7/8</p>
                <span>进取之路，怎能有后顾之忧！本周六至周日，百得利海淀奥迪唯你开启瞻前不必顾后“A6L零压力购车礼季”现在购买奥迪A6L，最高可享12.6万元优惠！另有30台特惠车型等你来选，还可尊享36期金融0利率，置换再享0购置税、0保险。现在购车还可享受最高1500元的加油卡，再赠3... <a href="资讯1.html">详细>></a></span>    <hr />
            </div>

            <div>
                <p><a href="">一场买家与卖家面对面交易的 二手车直卖会 免费报名中...... </a></p>
                <p>来源：  2016/7/8</p>
                <span>7月9日——7月10日 一场买家与卖家面对面交易的 二手车直卖会 免费报名中...... ▼▼▼ ▼▼▼ 具体流程 报名须知： 1、周一至周五期间到先锋路展厅或长江路展厅针对参展车辆进行免费检测，检测合格车辆即可参展。  <a href="">详细>></a></span>    <hr />
            </div>

            <div>
                <p><a href="">定义完美跑车 体验捷豹F-TYPE四驱敞篷版 </a></p>
                <p>来源：  2016/7/8</p>
                <span>跑车一直是男人的兴奋剂，一辆合格的跑车应该具备几个最基本的条件：迷人的外形、充沛的动力、精准的操控和醉人的声浪，如果再有个敞篷版就更完美了。这时你会发现，捷豹F-Type仿佛满足这所有的要求。 在一开始的时候，捷豹设计团队知道要打造一款完美的双门跑车 <a href="">详细>></a></span>    <hr />
            </div>


            <div>
                <p><a href="">宁利涛：签约行认证 搜狐二手车做最好的媒体信息服务平台</a></p>
                <p>来源：  2016/7/8</p>
                <span>跑车一直是男人的兴奋剂，一辆合格的跑车应该具备几个最基本的条件：迷人的外形、充沛的动力、精准的操控和醉人的声浪，如果再有个敞篷版就更完美了。这时你会发现，捷豹F-Type仿佛满足这所有的要求。 在一开始的时候，捷豹设计团队知道【编者按】2016年7月2日至5日，2016CUCA中国二手车大会暨首届品牌认证二手车展在重庆召开，大会将包含以"诚立信 逐大势"为主题的行业峰会，以"二手车，有品质的汽车生活"为主题的首届品牌认证二手车展两大内容。这场二手车行业精英云集的盛会，力图在政策利好的历史机... <a href="">详细>></a></span>    <hr />
            </div>



        </div>

        <!--左-->


        <!--  右 -->
        <div   id="right"></div>
        <!--  右 -->



    </div>


    <div style="clear:both;"></div>

    <div  id="copyright" > <p > @copyright   湖北工业大学小分队</p></div>

</div>




<div id="div3"><div id="div5"></div><hr /></div>
</body>
</html>

