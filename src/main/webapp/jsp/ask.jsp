<%--
  Created by IntelliJ IDEA.
  User: disneyland
  Date: 7/13/16
  Time: 11:20 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sum.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="uikit/css/uikit.gradient.min.css" rel="stylesheet" type="text/css"/>
    <script src="uikit/js/jquery-2.1.1.min.js"></script>
    <script src="uikit/js/uikit.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>主页</title>
    <style type="text/css">
        #top{
            padding-top:8px;
            height:40px;
        }
        #top_btn{
            padding-top:10px;
            height:inherit;
            padding-left:1150px;
        }
        #menu{
            margin-top:10px;
            background-color:#CCC;
            height:41px;
        }
        #nav_list{
            width:760px;
            margin-left:290px;
            position:absolute;
        }
        .nav_list{
            width:150px;
        }
        .nav_list a{
            text-align:center;
        }
        #content{

            width:750px;
            height:1200px;
            margin-top:50px;
            left:300px;
            float:right;
            position:absolute;
        }
        #input{
            float:left;
        }
        #ask{
            margin-left:60px;
            margin-top:20px;
            width:500px;
            height:100px;
        }
        #text_btn{
            margin-top:20px;
            margin-right:50px;
            float:right;
        }
        #text1{
            width:500px;
            height:300px;
            margin-left:40px;
            margin-top:10px;
        }
        #text2{
            width:500px;
            height:300px;
            margin-top:10px;
            margin-left:40px;
        }
        #text3{
            width:500px;
            height:300px;
            margin-top:10px;
            margin-left:40px;
        }
        .que_list{
            padding-left:10px;
            padding-top:10px;
        }
        .que_list a{
            color:#333;
        }
        .que_list a:hover{
            color:#F90;
        }
    </style>
</head>

<body>

<div id="top">
    <div id="top_btn">
        <input type="button" class="uk-button" onclick="location.href='./login'" value="登陆"/>
        <input type="button" class="uk-button" onclick="location.href='./register'" value="注册"/>
    </div>
</div>

<div  id="login">
    <div  style="float:right">
        <button class="uk-button" type="button" style="background-color:#0080FF; margin-top:10px; color:#999;">登陆</button>
        <button  class="uk-button" type="button" style="margin-right:10px; margin-top:10px; background-color:#0080FF; color:#999;" >注册</button>
    </div>
</div>
<div id="photo">湖工二手车市场</div>
<div id="navigation">
    <nav class="uk-navbar" style="background-color:#1166AC; color:#CCC;">
        <ul class="uk-navbar-nav" >
            <li ><a href="./home" target="_self">首页</a></li>
            <li><a href="./buycar">我要买车</a></li>
            <li class="uk-parent"><a href="./sellcar">我要卖车</a></li>
            <li><a href="./news">新闻</a></li>
            <li><a href="./ask">二手车问答</a></li>
        </ul>
    </nav>
</div>

<div id="content">
    <div id="ask">
        <form action="./ask" method="post">
            <div id="input">
                <textarea name="question" style="font-size:22px" cols="30" rows="3" cols="20" wrap="soft" placeholder="请在此输入您的问题！"></textarea>
            </div>
            <div id="text_btn"><input type="button" class=" uk-button-large" value="提交"/></div>
        </form>
    </div>

    <div id="text1">
        <h2>热门问题</h2><hr />
        <ul class="uk-list">
            <li class="que_list"><a href="">问：新车前500公里行驶时都需要注意什么<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：汽车低速行驶时前轮摆动是哪里的问题<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：车内有异响该怎么办<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：在松合离合器时有些抖动什么原因<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：汽车碰撞试验是星级越高越安全吗<i class=" uk-icon-question"></i></a></li>
        </ul>
    </div>
    <div id="text2">
        <h2>未解决的</h2><hr />
        <ul class="uk-list">
            <li class="que_list"><a href="">问：新车前500公里行驶时都需要注意什么<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：汽车低速行驶时前轮摆动是哪里的问题<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：车内有异响该怎么办<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：在松合离合器时有些抖动什么原因<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：汽车碰撞试验是星级越高越安全吗<i class=" uk-icon-question"></i></a></li>
        </ul>
    </div>
    <div id="text3">
        <h2>已解决的</h2><hr />
        <ul class="uk-list">
            <li class="que_list"><a href="">问：我的车磨合期油耗为什么这么高这个正常吗<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：三厢车比两厢车多个车尾就更加安全吗<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：购车为上班代步建议买涡轮增压车型吗<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：自主品牌的车型是不是小毛病更多些啊<i class=" uk-icon-question"></i></a></li>
            <li class="que_list"><a href="">问：CVT变速箱的主要优势体现在哪些方面<i class=" uk-icon-question"></i></a></li>
        </ul>
    </div>
</div>
</body>
</html>
