<%--
  Created by IntelliJ IDEA.
  User: disneyland
  Date: 7/13/16
  Time: 10:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>注册页面</title>
    <script type="text/javascript">
        function namaTips(){
            var tips=document.getElementById("tips");
            tips.innerHTML="<font color='blue'><b>用户名由字母和数字组成</b></font> ";
        }

        function pwdTips(){
            var tips=document.getElementById("tips");
            tips.innerHTML="<font color='blue'><b>密码大小为4-11位</b></font>";
        }

        function pwd2Tips(){
            var tips=document.getElementById("tips");
            tips.innerHTML="<font color='blue'><b>请再次输入密码</b></font>";
        }

        function checkname(){
            var user=document.getElementById("user");
            var tips=document.getElementById("tips");
            var zhengze=/^[a-zA-Z0-9]+$/;
            if(user.val().length==0){
                var submit = document.getElementById("submit");
                tips.innerHTML="<font color='red'><b>用户名不能为空</b></font>";
                return false;
            }else if(zhengze.test(user.val())){
                return true;
            }else{
                tips.innerHTML="<font color='red'><b>用户名格式错误</b></font>";
                return false;
            }
        }

        function checkpwd(){
            var passwd =document.getElementById("passwd");
            var tips=document.getElementById("tips");
            var len = passwd.value.length;
            if(passwd.value.length==0){
                tips.innerHTML="<font color='red'><b>密码不能为空</b></font>";
                return false;
            }else if(len<4 || len>11){
                tips.innerHTML="<font color='red'><b>密码长度不合法</b></font>";
                return false;
            }else{
                return true;
            }
        }

        function checkrepwd(){
            var passwd =document.getElementById("passwd");
            var passwd2=document.getElementById("passwd2");
            var tips=document.getElementById("tips");
            var len = passwd2.value.length;
            if(passwd2.value.length==0){
                tips.innerHTML="<font color='red'><b>密码不能为空</b></font>";
                return false;
            }else if(passwd2.value==passwd.value){
                return true;
            }else{
                tips.innerHTML="<font color='red'><b>两次密码输入不一致</b></font>";
                return false;
            }
        }

        function checkphone(){
            var phone = document.getElementById("phone");
            var tips=document.getElementById("tips");
            var reg = /^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)\\d{8}+$/;
            if(reg.test(phone.value)){
                return true;
            }else{
                tips.innerHTML="<font color='red'><b>请输入正确的手机号码</b></font>";
                return false;
            }
        }

        function checkInput(){
            return checkname()&&checkpwd()&&checkrepwd()&&checkphone();
        }
    </script>
</head>
<body>
<h1>CarTrade<sup>WebSite</sup></h1>
<div class="login" style="margin-top:50px;">
    <div class="header">
        <div class="switch" id="switch">
            <a class="switch_btn" id="switch_login" >快速注册</a>
        </div>
    </div>

    <div class="qlogin" id="qlogin" >
        <div class="web_login">
            <form name="form2" id="regUser" accept-charset="utf-8"
                  action="./register" method="post" onsubmit="return checkInput()">
                <ul class="reg_form" id="reg-ul">
                    <div id="userCue" class="cue">
                        <label id="tips">快速注册请注意格式</label>
                    </div>
                    <li><label for="user" class="input-tips2">账户名：</label>
                        <div class="inputOuter2">
                            <input id="user" name="account" maxlength="16" class="inputstyle2"
                                   type="text" onclick="namaTips()">
                        </div></li>
                    <li><label for="passwd" class="input-tips2">密码：</label>
                        <div class="inputOuter2">
                            <input id="passwd" name="password" maxlength="16"
                                   class="inputstyle2" type="password" onclick="pwdTips()">
                        </div></li>
                    <li><label for="passwd2" class="input-tips2">确认密码：</label>
                        <div class="inputOuter2">
                            <input id="passwd2" name="passwd2" maxlength="16"
                                   class="inputstyle2" type="password" onclick="pwd2Tips()">
                        </div></li>

                    <li><label for="nickname" class="input-tips2">昵称：</label>
                        <div class="inputOuter2">

                            <input id="nickname" name="name" maxlength="10"
                                   class="inputstyle2" type="text">
                        </div></li>

                    <li><label for="phone" class="input-tips2">手机号码：</label>
                        <div class="inputOuter2">
                            <input id="phone" name="telphone" maxlength="16"
                                   class="inputstyle2" type="text">
                        </div></li>
                    <li><label  class="input-tips2">邮箱：</label>
                        <div class="inputOuter2">
                            <input id="mail" name="mail" maxlength="16"
                                   class="inputstyle2" type="text">
                        </div>
                    </li>
                    <li>


                    <li>
                        <div class="inputArea">
                            <input id="reg" style="margin-top: 10px; margin-left: 100px;"
                                   class="button_blue" value="提交注册" type="submit">
                            <div id="toLogin" class="toLogin">
                                <a href="./login">　已有账号？</a>
                            </div>
                        </div>

                    </li>
                </ul>
            </form>
        </div>
    </div>
</div>
</body>
</html>
