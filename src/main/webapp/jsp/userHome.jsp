<%--
  Created by IntelliJ IDEA.
  User: disneyland
  Date: 7/13/16
  Time: 7:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        #content{
            background-color:#CF9;
            width:1000px;
            height:1500px;
            margin-top:20px;
            left:220px;
            position:absolute;
        }
        #line{
            background-color:#CCC;
            margin-top:5px;
            height:1px;
            width:800px;;
            margin-left:20px;
        }
        #line2{
            background-color:#CCC;
            width:1px;
            height:1000px;
            margin-left:20px;
            padding-top:10px;
            float:left;
        }
        #show{
            width:700px;
            height:600px;
            margin-top:20px;
            margin-left:80px;
        }
        #showAccount{
            margin-top:40px;
            margin-left:80px;
            font-size:24px;
        }
        #show h2{
            text-align:center;
        }
        #showPwd{
            margin-top:40px;
            margin-left:80px;
            font-size:24px;
        }
        #showName{
            margin-top:40px;
            margin-left:80px;
            font-size:24px;
        }
        #showTel{
            margin-top:40px;
            margin-left:80px;
            font-size:24px;
        }
        #showMail{
            margin-top:40px;
            margin-left:80px;
            font-size:24px;
        }
        #line3{
            background-color:#CCC;
            margin-top:40px;
            height:1px;
            width:900px;;
        }
        #show2{
            width:700px;
            height:200px;
            margin-top:20px;
            margin-left:20px;
        }
        #show2 h2{
            text-align:center;
        }
        #show2 span{
            margin-top:40px;
            margin-left:80px;
            font-size:24px;
        }
        .uk-nav ul {
            display: none;
        }

        .uk-nav :hover ul {
            display: block;
        }

        .uk-nav ul li {
            float: right;
            width: 30px;
        }

        .uk-nav {
            width: 150px;
            height: 40px;
        }

        .uk-thumbnail1 {
            display: inline-block;
            max-width: 24%;
            box-sizing: border-box;
            margin: 0;
            padding: 4px;
            border: 1px solid #ddd;
            background: #fff;
            border-radius: 4px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .05);
        }
        #photo {
            height: 200px;
            width: 100%;
            background-image: url(image/%E6%A0%A1%E9%97%A8.jpg);
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<div id="content">
    <div id="line"></div>
    <div id="line2"></div>
    <div id="photo">湖北工业大学跳蚤街</div>
    <div id="navigation">
        <nav class="uk-navbar" style="background-color:#1166AC; color:#CCC;">
            <ul class="uk-navbar-nav">
                <li><a href="./home" target="_self">首页</a></li>
                <li><a href="./buycar">我要买车</a></li>
                <li class="uk-parent"><a href="./sellcar">我要卖车</a></li>
                <li><a href="./news">新闻</a></li>
                <li><a href="./ask">二手车问答</a></li>
            </ul>
        </nav>
    </div>
    <div id="show">
        <h2>您的个人信息如下</h2>
        <div id="showAccount">
            <span class="showMsg">账号：<label id="account">显示账号标签</label> </span>
        </div>
        <div id="showPwd">
            <span class="showMsg">密码：<label id="pwd">********</label> <input type="button" class="uk-button" value="修改密码"/></span>
        </div>
        <div id="showName">
            <span class="showMsg">用户名：<label id="name">显示用户名标签</label></span>
        </div>
        <div id="showTel">
            <span class="showMsg">手机号：<label id="tel">显示手机号标签</label><input type="button" class="uk-button" value="修改手机号"/></span>
        </div>
        <div id="showMail">
            <span class="showMsg">邮箱：<label id="mail">显示邮箱标签</label><input type="button" class="uk-button" value="修改邮箱"/></
        </div>
    </div>
    <div id="line3"></div>
    <div id="show2">
        <h2>您的购车记录如下</h2>
        <span>记录条数1</span><br />
        <span>记录条数2</span><br />
        <span>记录条数3</span><br />
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
