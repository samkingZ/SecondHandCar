<%--
  Created by IntelliJ IDEA.
  User: disneyland
  Date: 7/13/16
  Time: 12:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="./uikit/css/uikit.gradient.min.css" rel="stylesheet" type="text/css"/>
    <script src="./uikit/js/jquery-2.1.1.min.js"></script>
    <script src="./uikit/js/uikit.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style  type="text/css">

        #login{
            height:50px;
            width:100%;
        }


        #photo{height:200px;
            width:100%;
            background-image:url(image/%E6%A0%A1%E9%97%A8.jpg);}

        #navigation{background-color:#000;}


        #left{
            margin-top:2px;
            width:30%;
            float:left;

        }


        #right{
            margin-top:2px;
            width:68%;
            float:right;


        }


        #div1{
            /*      position:absolute; */
            float:left;
            margin-left:10px;
            width:10%;
        }



        #div2{

            float:left;
            width:78%;}


        #div3{  float:left; margin-right:10px;
            width:10%;}



        #div4{height:50px;
            width:100%;
            background-color:#FFF;}


        #div5{height:50px;
            background-color:#FFF;
            width:100%;}



        #sousuo{
            height:40px;
            text-align:left;
            background-color:#06F;
            color:#FFF;
        }


        #sousuo1{
            height:40px;
            padding:3px;
            width:95%;
            background-color:#FFF;}


        #suo{
            height:100px;
            width:99%;
            border-color:#000;
            border-left:1px solid #999;
            border-right:1px solid #999;
            border-bottom:1px solid #999;
        }

        #lianjie{ height:500px;
            width:100%;
            border-color:#000;
            margin-top:10px;
            color:#FFF;
            border-left:1px solid #999;
            border-right:1px solid #999;
            border-bottom:1px solid #999;
            border-top:1px solid #999;}


        #lianjie1{
            height:40px;
            padding:1px;
            width:99%;
            background-color:#06F;
        }


        #leftright{  width:100%;

            margin-top:2px;
        }

        #copyright{
            text-align:center;
            height:40px;
            width:100%;
            margin-bottom:2px;
        }


        #xiaomen{ height:200px;
            width:100%;
            background-image:url(%E6%A0%A1%E9%97%A8.jpg);
        }


        .uk-nav  ul  { display:none;}
        .uk-nav  :hover  ul {display:block;}
        .uk-nav   ul  li { float:right;
            width:30px; }
        .uk-nav{ width:150px; height:40px;}

    </style>
    <title>我要卖车</title>
</head>

<body style="overflow-y:scroll">
<div id="div1"><div id="div4"></div><hr /></div>
<div id="div2">
    <div  id="login">  <div  style="float:right"> <div style="clear:both;"></div>
        <a href="./login" class="uk-button" type="button" style="background-color:#0080FF; margin-top:10px; color:#999;">登陆</a>
        <a href="./register" class="uk-button" type="button" style="margin-right:10px; margin-top:10px; background-color:#0080FF; color:#999;" >注册</a>
    </div>
    </div>
    <div id="photo">湖工二手车市场</div>
    <div id="navigation">
        <nav class="uk-navbar" style="background-color:#1166AC; color:#CCC;">
            <ul class="uk-navbar-nav" >
                <li ><a href="./home" target="_self">首页</a></li>
                <li><a href="./buycar">我要买车</a></li>
                <li class="uk-parent"><a href="./sellcar">我要卖车</a></li>
                <li><a href="./news">新闻</a></li>
                <li><a href="./ask">二手车问答</a></li>
            </ul>
        </nav>
    </div>
    <div id="leftright" >


        <div style="margin-top:20px;">  <h3>请填写你的二手车信息</h3></div><hr />



        <form action="./sellcar" method="post">
            <div>
                <p style=" margin-left:60px">       车型:<select style="margin-left:30px; "><option > 请选择品牌</option>
                    <option>大众</option>
                    <option>本田</option>
                    <option>奔驰</option>
                    <option>宝马</option>

                </select>

                    <select  style="margin-left:30px">

                        <option>请选择车系</option>
                        <option>polo</option>
                        <option>golf</option>
                        <option>cc</option>
                        <option>朗逸</option>
                        <option>捷达</option>
                    </select>


                    <select  style="margin-left:30px">
                        <option>请选择车型</option>
                        <option>豪华型</option>
                        <option>基本型</option>
                        <option>舒适型</option>
                        <option>经济型</option>
                    </select>
                </p>
            </div>

            <div>
                <p style="margin-left:60px">车牌所在地：
                    <select name="getLicensePlace">
                        <option>武汉</option>
                        <option>苏州</option>
                        <option>无锡</option>
                        <option>九江</option>
                    </select>
                </p>
            </div>
            <div>   <p style="margin-left:60px">行程里程： <input name="mileage" type="text"     />   <span> 万公里</span></p></div>
    </div>
    <div>   <p style="margin-left:60px">首次上牌时间： <input  name="getLicenseDate" type="text"     />  </p></div>

    <div>   <p style="margin-left:60px">排量： <input  name="emissionStandard" type="text"     />  </p></div>

    <div>   <p style="margin-left:60px">预售价格： <input  name="price" type="text"     />   <span> 万元</span></p></div><hr />


    <div>    车辆实拍照片：<input type="file">点击添加实拍照片</input>上传实拍照片可提高一倍售出成功率，最多上传 10 张，按Ctrl可多选图片<br />
        <img   style="margin-left:100px;"  src="image/车上传模板.png"/></div>
    <div style="text-align: center"><button style="" class="uk-button" type="submit">提交</button></div>

</div>

</form>





</div>



<div style="clear:both;"></div>

<div  id="copyright" > <p > @copyright   secondCar</p></div>

</div>



<div id="div3"><div id="div5"></div><hr /></div>
</body>
</html>

