<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: disneyland
  Date: 7/13/16
  Time: 12:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="./uikit/css/uikit.gradient.min.css" rel="stylesheet" type="text/css"/>
    <script src="./uikit/js/jquery-2.1.1.min.js"></script>
    <script src="./uikit/js/uikit.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">

        #login {
            height: 50px;
            width: 100%;
        }

        #photo {
            height: 200px;
            width: 100%;
            background-image: url(image/%E6%A0%A1%E9%97%A8.jpg);
        }

        #navigation {
            background-color: #000;
        }

        #left {
            margin-top: 2px;
            width: 30%;
            float: left;

        }

        #right {
            margin-top: 2px;
            width: 68%;
            float: right;

        }

        #div1 {
            /*      position:absolute; */
            float: left;
            margin-left: 10px;
            width: 10%;
        }

        #div2 {

            float: left;
            width: 78%;
        }

        #div3 {
            float: left;
            margin-right: 10px;
            width: 10%;
        }

        #div4 {
            height: 50px;
            width: 100%;
            background-color: #FFF;
        }

        #div5 {
            height: 50px;
            background-color: #FFF;
            width: 100%;
        }

        #sousuo {
            height: 40px;
            text-align: left;
            background-color: #06F;
            color: #FFF;
        }

        #sousuo1 {
            height: 40px;
            padding: 3px;
            width: 95%;
            background-color: #FFF;
        }

        #suo {
            height: 200px;
            width: 99%;
            border-color: #000;
            border-left: 1px solid #999;
            border-right: 1px solid #999;
            border-bottom: 1px solid #999;
            border-top: 1px solid #999;
        }

        #lianjie {
            height: 500px;
            width: 100%;
            border-color: #000;
            margin-top: 10px;
            color: #FFF;
            border-left: 1px solid #999;
            border-right: 1px solid #999;
            border-bottom: 1px solid #999;
            border-top: 1px solid #999;
        }

        #lianjie1 {
            height: 40px;
            padding: 1px;
            width: 99%;
            background-color: #06F;
        }

        #leftright {
            width: 100%;

            margin-top: 2px;
        }

        #copyright {
            text-align: center;
            height: 40px;
            width: 100%;
            margin-bottom: 2px;
        }

        #xiaomen {
            height: 200px;
            width: 100%;
            background-image: url(%E6%A0%A1%E9%97%A8.jpg);
        }

        .uk-nav ul {
            display: none;
        }

        .uk-nav :hover ul {
            display: block;
        }

        .uk-nav ul li {
            float: right;
            width: 30px;
        }

        .uk-nav {
            width: 150px;
            height: 40px;
        }

        .uk-thumbnail1 {
            display: inline-block;
            max-width: 24%;
            box-sizing: border-box;
            margin: 0;
            padding: 4px;
            border: 1px solid #ddd;
            background: #fff;
            border-radius: 4px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .05);
        }

    </style>
    <title>我要买车</title>
</head>

<body style="overflow-y:scroll">
<div id="div1">
    <div id="div4"></div>
    <hr/>
</div>
<div id="div2">
    <div id="login">
        <div style="float:right">
            <div style="clear:both;"></div>

            <a href="./login" class="uk-button" type="button"
               style="background-color:#0080FF; margin-top:10px; color:#999;">登陆</a>
            <a href="./register" class="uk-button" type="button"
               style="margin-right:10px; margin-top:10px; background-color:#0080FF; color:#999;">注册</a>
        </div>
    </div>
    <div id="photo">湖北工业大学跳蚤街</div>
    <div id="navigation">
        <nav class="uk-navbar" style="background-color:#1166AC; color:#CCC;">
            <ul class="uk-navbar-nav">
                <li><a href="./home" target="_self">首页</a></li>
                <li><a href="./buycar">我要买车</a></li>
                <li class="uk-parent"><a href="./sellcar">我要卖车</a></li>
                <li><a href="./news">新闻</a></li>
                <li><a href="./ask">二手车问答</a></li>
            </ul>
        </nav>
    </div>
    <div id="leftright">
        <%--<div id="suo">

            <div  style="margin-top:20px"><p><span style="margin-left:20px;">品牌：</span><a style="margin-left:40px;" href="">大众</a>
                <a style="margin-left:20px;" href="">福特</a>
                <a style="margin-left:20px;" href="">别克</a>
                <a style="margin-left:20px;" href="">雪佛兰</a>
                <a style="margin-left:20px;" href="">比亚迪</a>
                <a style="margin-left:20px;" href="">现代</a>
                <a style="margin-left:20px;" href="">奔驰</a>
                <a style="margin-left:20px;" href="">宝马</a>
                <a style="margin-left:20px;" href="">斯柯达</a>
                <a style="margin-left:20px;" href="">日产</a>
                <a style="margin-left:20px;" href="">五菱</a>
                <a style="margin-left:20px;" href="">奥迪</a>
                <a style="margin-left:20px;" href="">丰田</a>
                <a style="margin-left:20px;" href="">起亚</a>
                <a style="margin-left:20px;" href="">标志</a>
                <a style="margin-left:20px;" href="">奇瑞</a>
                <a style="margin-left:20px;" href="">本田</a>
            </p></p></div>



            <div><p><span style="margin-left:20px;">价格：</span><a style="margin-left:40px;" href="">大众</a>
                <a style="margin-left:20px;" href="">5万以下</a>
                <a style="margin-left:20px;" href="">5-10万</a>
                <a style="margin-left:20px;" href="">10-15万</a>
                <a style="margin-left:20px;" href="">15到20万</a>
                <a style="margin-left:20px;" href="">20-30万</a>
                <a style="margin-left:20px;" href="">30-40万</a>
                <a style="margin-left:20px;" href="">40-60万</a>
                <a style="margin-left:20px;" href="">60万以上</a>

            </p></p></div>


            <div><p><span style="margin-left:20px;">车龄：</span><a style="margin-left:40px;" href="">大众</a>
                <a style="margin-left:20px;" href="">5年以上</a>
                <a style="margin-left:20px;" href="">5年以内</a>
                <a style="margin-left:20px;" href="">三年以内</a>
                <a style="margin-left:20px;" href="">一年以内</a>


            </p></p></div></div>

        <div   style="clear:both;"></div>

--%>
        <div id="jianjie">

            <c:forEach items="${requestScope.secondCarList}" var="secondCar">

                <div class="uk-thumbnail1" style="margin-left:5px">
                    <a href="./detail?id=${secondCar.id}"> <img src="16sucai.com/images/2.jpg" alt=""></a>
                    <div class="uk-thumbnail-caption"><p>
                        <p>大众</p>
                     ${secondCar.getLicenseDate}上牌 | 行驶${secondCar.mileage}万公里 </p>
                        <p>${secondCar.price}万</p></div>
                </div>
            </c:forEach>
            ${pageContext.request.getAttribute("pageNum")}
            <!--第一列图片-->
            <!-- This is a div as a thumbnail with a caption -->

            <!--第一列图片--></div>


    </div>


    <div style="clear:both;"></div>


    <div id="copyright"><p> @copyright 湖北工业大学小分队</p></div>

</div>


<div id="div3">
    <div id="div5"></div>
    <hr/>
</div>
</body>
</html>