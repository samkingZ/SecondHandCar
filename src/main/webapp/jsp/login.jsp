<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>登陆页面</title>
</head>
<body>
<h1>CarTrade<sup>WebSite</sup></h1>
<div class="login" style="margin-top:50px;">
    <div class="header">
        <div class="switch" id="switch"><a class="switch_btn_focus" id="switch_qlogin">快速登录</a>
		
        </div>
    </div>
    <div class="web_qr_login" id="web_qr_login" style="display: block; height: 235px;">
            <!--登录-->
            <div class="web_login" id="web_login">               
              <div class="login-box">
			   
			<div class="login_form">
				<form action="./login" name="loginform" id="login_form" class="loginForm" method="post">
				
                <div class="uinArea" id="uinArea">
                <label class="input-tips" for="u">帐号：</label>
                <div class="inputOuter" id="uArea">
                    <input id="u" name="account" class="inputstyle" type="text">
                </div>
                </div>
				
                <div class="pwdArea" id="pwdArea">
               <label class="input-tips" for="p">密码：</label>
               <div class="inputOuter" id="pArea">
                    
                    <input id="p" name="password" class="inputstyle" type="password">
                </div>
                </div>
               
                <div style="padding-left:50px;margin-top:20px;"><input value="登 录" style="width:150px;" class="button_blue" type="submit"></div>
					<div id="toRegist" class="toRegist"><a href="./register">　没有账号？</a></div>
              </form>
           </div>
           
            	</div>
               
            </div>
            <!--登录end-->
  </div>

 
</div>
</body></html>